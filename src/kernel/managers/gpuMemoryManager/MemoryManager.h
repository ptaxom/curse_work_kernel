#ifndef KERNEL_MANAGER_GPU_MEMORY_MANAGER
#define KERNEL_MANAGER_GPU_MEMORY_MANAGER

#include "kernel/containers/GPU/SharedPtr/SharedPtr.h"
#include <stdint.h>
#include "kernel/containers/GPU/ImageArray/ImageArray.h"
#include "kernel/Utility/Utility.h"

namespace kernel
{

namespace Managers
{

namespace GPU
{


class MemoryManager
{
public:

    // Allocates pitched gpu stored array of type T which represents image WIDTHxHEIGHT with sizeof(T) * coefficient bytes per pixel
    template <typename T>
    static kernel::Container::GPU::ImageArray<T> allocate_pitched_ImageArray(const size_t width, const size_t height, const size_t coefficient)
    {
        size_t bytes_per_pixel = coefficient * sizeof(T);
        size_t memory_size = width * height * bytes_per_pixel;
        if (memory_size == 0)
            throw Utility::EmptyImageException("Cannot get GPU::ImageArray<T> from empty image.");

        T *device_array = nullptr;
        size_t device_pitch = 0;
        if (cudaMallocPitch((void **)&device_array, &device_pitch, bytes_per_pixel * width, height) != cudaSuccess)
        {
            std::cerr << cudaGetErrorName(cudaGetLastError()) << std::endl;
            throw Utility::CUDAMallocException("Cannot allocate GPU::ImageArray<T>");
        }

        auto device_ptr = kernel::Container::GPU::make_cuda_shared(device_array);        
        kernel::Container::GPU::ImageArray<T> array(device_pitch, device_ptr, bytes_per_pixel);
        kernel::Container::GPU::SingletonCounter::allocate();
        return array;
    }

    template <typename T>
    static void copy_to_pitched_ImageArray(kernel::Container::GPU::ImageArray<T> &array, const std::shared_ptr<T> pixels, const size_t width, const size_t height, const size_t coefficient)
    {
        size_t bytes_per_pixel = coefficient * sizeof(T);
        if (pixels.get() == nullptr)
            throw Utility::EmptyImageException("Cannot copy GPU::ImageArray<T> from empty image.");

        if (array.device_array.get() == nullptr || array.pitch == 0 || array.bytes_per_pixel == 0)
            throw Utility::EmptyImageException("Cannot copy GPU::ImageArray<T> to empty array.");

        if (cudaMemcpy2D(array.device_array.get(), array.pitch,
                         pixels.get(), bytes_per_pixel * width,
                         bytes_per_pixel * width, height,
                         cudaMemcpyHostToDevice) != cudaSuccess)
        {
            std::cerr << cudaGetErrorName(cudaGetLastError()) << std::endl;
            throw Utility::CUDAMemcpyException("Cannot copy GPU::ImageArray<T> from host to device");
        }
    }

    template <typename T>
    static void load_pitched_ImageArray_to_raw_array(const kernel::Container::GPU::ImageArray<T> &array, std::shared_ptr<T> out_array, const size_t width, const size_t height, const size_t coefficient)
    {
        size_t bytes_per_pixel = coefficient * sizeof(T);
        if (out_array.get() == nullptr)
            throw Utility::EmptyImageException("Cannot copy GPU::ImageArray<T> to empty array.");

        if (array.device_array == nullptr || array.pitch == 0 || array.bytes_per_pixel == 0)
            throw Utility::EmptyImageException("Cannot copy GPU::ImageArray<T> to empty array.");

        if (cudaMemcpy2D(out_array.get(), bytes_per_pixel * width,
                         array.device_array.get(), array.pitch,
                         bytes_per_pixel * width, height,
                         cudaMemcpyDeviceToHost) != cudaSuccess)
        {
            std::cerr << "Error: " << cudaGetErrorName(cudaGetLastError()) << std::endl;
            throw Utility::CUDAMemcpyException("Cannot copy GPU array from device to host");
        }
    }

    template <typename T>
    static std::shared_ptr<T> allocate_raw_array(const size_t elements)
    {
        size_t memory_size = elements * sizeof(T);
        if (memory_size == 0)
            throw Utility::EmptyRawArrayException("Cannot allocate empty raw array.");

        T *device_array = nullptr;
        if (cudaMalloc((void **)&device_array, memory_size) != cudaSuccess)
        {
            std::cerr << cudaGetErrorName(cudaGetLastError()) << std::endl;
            throw Utility::CUDAMallocException("Cannot allocate empty raw array.");
        }
        std::shared_ptr<T> device_ptr = kernel::Container::GPU::make_cuda_shared(device_array);        
        kernel::Container::GPU::SingletonCounter::allocate();
        return device_ptr;
    }

    template <typename T>
    static void copy_raw_array_to_host(const std::shared_ptr<T> device_ptr, std::shared_ptr<T> host_ptr, const size_t elements)
    {
        size_t memory_size = elements * sizeof(T);
        if (memory_size == 0 || !device_ptr.get() || !host_ptr.get())
            throw Utility::EmptyRawArrayException("Cannot copy empty raw array.");

        if (cudaMemcpy(host_ptr.get(), device_ptr.get(), memory_size, cudaMemcpyDeviceToHost) != cudaSuccess)
        {
            std::cerr << cudaGetErrorName(cudaGetLastError()) << std::endl;
            throw Utility::CUDAMemcpyException("Cannot copy raw array.");
        }
    }

    template <typename T>
    static void copy_raw_array_to_host(const std::shared_ptr<T> device_ptr, T *host_ptr, const size_t elements)
    {
        size_t memory_size = elements * sizeof(T);
        if (memory_size == 0 || !device_ptr.get() || !host_ptr)
            throw Utility::EmptyRawArrayException("Cannot copy empty raw array.");

        if (cudaMemcpy(host_ptr, device_ptr.get(), memory_size, cudaMemcpyDeviceToHost) != cudaSuccess)
        {
            std::cerr << cudaGetErrorName(cudaGetLastError()) << std::endl;
            throw Utility::CUDAMemcpyException("Cannot copy raw array.");
        }
    }

    template <typename T>
    static void copy_raw_array_to_device(std::shared_ptr<T> device_ptr, const std::shared_ptr<T> host_ptr, const size_t elements)
    {
        size_t memory_size = elements * sizeof(T);
        if (memory_size == 0 || !device_ptr.get() || !host_ptr.get())
            throw Utility::EmptyRawArrayException("Cannot copy empty raw array.");

        if (cudaMemcpy(device_ptr.get(), host_ptr.get(), memory_size, cudaMemcpyHostToDevice) != cudaSuccess)
        {
            std::cerr << cudaGetErrorName(cudaGetLastError()) << std::endl;
            throw Utility::CUDAMemcpyException("Cannot copy raw array.");
        }
    }

    template <typename T>
    static void copy_ImageArray_to_ImageArray(const kernel::Container::GPU::ImageArray<T> &source, kernel::Container::GPU::ImageArray<T> &dest, const size_t width, const size_t height, const size_t coefficient)
    {
        size_t bytes_per_pixel = coefficient * sizeof(T);
        if (source.device_array.get() == nullptr || source.pitch == 0 || source.bytes_per_pixel == 0)
            throw Utility::EmptyImageException("Cannot copy GPU::ImageArray<T> from empty array.");

        if (dest.device_array.get() == nullptr || dest.pitch == 0 || dest.bytes_per_pixel == 0)
            throw Utility::EmptyImageException("Cannot copy GPU::ImageArray<T> to empty array.");

        if (cudaMemcpy2D(dest.device_array.get(), dest.pitch,
                         source.device_array.get(), source.pitch,
                         bytes_per_pixel * width, height,
                         cudaMemcpyDeviceToDevice) != cudaSuccess)
        {
            std::cerr << cudaGetErrorName(cudaGetLastError()) << std::endl;
            throw Utility::CUDAMemcpyException("Cannot copy GPU::ImageArray<T> from device to device");
        }
    }

};

}; // namespace GPU
}; // namespace Managers
}; // namespace kernel

#endif