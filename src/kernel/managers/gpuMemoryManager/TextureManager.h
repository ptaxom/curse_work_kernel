#ifndef KERNEL_MANAGER_GPU_TEXTURE_MANAGER
#define KERNEL_MANAGER_GPU_TEXTURE_MANAGER

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include "kernel/containers/GPU/RGBAImage/RGBAImage.h"

namespace kernel
{

namespace Managers
{

namespace GPU
{

class TextureManager
{
public:
    template <typename T, int dim, cudaTextureReadMode readMode>
    static void init_texture(texture<T, dim, readMode> &texture, const RGBAImage &image, enum cudaTextureAddressMode addressMode, bool normalized = false)
    {
        cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<int>();
        RGBAArray rgba_array = image.get_image_storage();

        texture.normalized = normalized;
        texture.filterMode = cudaFilterModePoint;
        texture.addressMode[0] = addressMode;
        texture.addressMode[1] = addressMode;

        uint32_t height = image.get_height(),
                 width = image.get_width();

        size_t offset;
        const void *device_ptr = rgba_array.get_device_array().get();
        if (cudaBindTexture2D(&offset, &texture,
                              device_ptr, &channelDesc,
                              width, height, // Maybe width in bytes
                              rgba_array.get_pitch()) != cudaSuccess)
        {
            throw Utility::CUDATextureException("Cannot bind 2D array to texture");
        }
    }
};
}; // namespace GPU
}; // namespace Managers
}; // namespace kernel

#endif