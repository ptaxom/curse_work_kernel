#ifndef KERNEL_CONTAINER_CONV_MATRIX
#define KERNEL_CONTAINER_CONV_MATRIX

#include <vector>
#include <initializer_list>
#include <cuda.h>
#include <cstring>

#include "kernel/Utility/Utility.h"
#include "kernel/containers/GPU/ImageArray/ImageArray.h"

namespace kernel
{
namespace Container
{

template <typename DType>
class ConvolutionMatrix
{
public:
    ConvolutionMatrix(const DType norm_coef_,
                      const std::initializer_list<
                          const std::initializer_list<DType>> &coefficients_rows) : norm_coef(norm_coef_)
    {
        conv_size = coefficients_rows.size();
        if (conv_size == 0)
            throw Utility::ContainerDimensionsException("Empty convolution matrix.");
        for (const auto &row : coefficients_rows)
            if (row.size() != conv_size)
                throw Utility::ContainerDimensionsException("Diffrent row sizes.");

        for (const auto &row : coefficients_rows)
            cpu_storage.push_back(std::vector<DType>(row));
    }

    ConvolutionMatrix(const DType norm_coef_,
                      std::vector<
                          std::vector<DType>> &coefficients_rows) : norm_coef(norm_coef_)
    {
        conv_size = coefficients_rows.size();
        if (conv_size == 0)
            throw Utility::ContainerDimensionsException("Empty convolution matrix.");
        for (const auto &row : coefficients_rows)
            if (row.size() != conv_size)
                throw Utility::ContainerDimensionsException("Diffrent row sizes.");

        for (const auto &row : coefficients_rows)
            cpu_storage.push_back(std::vector<DType>(row));
    }

    __device__ __host__ const DType get_coefficient() const
    {
        return norm_coef;
    }

    __device__ __host__ size_t get_conv_size() const
    {
        return conv_size;
    }

    kernel::Container::GPU::ImageArray<DType> get_gpu_array() const
    {
        using kernel::Managers::GPU::MemoryManager;
        std::shared_ptr<DType> host_ptr(new DType[conv_size * conv_size]);
        for (int i = 0; i < conv_size; i++)
            for (int j = 0; j < conv_size; j++)
                host_ptr.get()[i * conv_size + j] = cpu_storage[i][j];
        kernel::Container::GPU::ImageArray<DType> device_array = MemoryManager::allocate_pitched_ImageArray<DType>(conv_size, conv_size, 1);
        MemoryManager::copy_to_pitched_ImageArray<DType>(device_array, host_ptr, conv_size, conv_size, 1);
        return device_array;
    }

private:
    std::vector<std::vector<DType>> cpu_storage;
    DType norm_coef;
    size_t conv_size;
};

}; // namespace Container
}; // namespace kernel

#endif