#ifndef KERNEL_CONTAINER_I_IMAGE
#define KERNEL_CONTAINER_I_IMAGE

#include <stdint.h>

namespace kernel
{
namespace Container
{

class I_Image
{

public:
    I_Image(int32_t width_, uint32_t height_, uint8_t channels_) : width(width_), height(height_), channels(channels_)
    {
    }

    virtual ~I_Image() {}

    virtual uint32_t get_width() const
    {
        return width;
    }

    virtual uint32_t get_height() const
    {
        return height;
    }

    virtual uint32_t get_channels() const
    {
        return channels;
    }

    virtual bool is_empty() const
    {
        return width == 0 || height == 0 || channels == 0;
    }

protected:
    uint32_t width;
    uint32_t height;
    uint8_t channels;
};

}; // namespace Container
}; // namespace kernel

#endif