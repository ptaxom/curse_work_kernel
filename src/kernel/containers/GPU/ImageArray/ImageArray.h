#ifndef KERNEL_CONTAINERS_GPU_IMAGEARRAY
#define KERNEL_CONTAINERS_GPU_IMAGEARRAY

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include "kernel/containers/GPU/SharedPtr/SharedPtr.h"

namespace kernel
{

namespace Managers
{
namespace GPU
{
class MemoryManager;
};
}; // namespace Managers

namespace Container
{

class Image;

namespace GPU
{
class RGBAImage;

// Store type T array with memory pitch
template <typename T>
class ImageArray
{
public:

    __device__ inline size_t get_pixel_index(int x, int y) const
    {
        return y * pitch + bytes_per_pixel * x;
    }

    __device__ inline T *get_pixel(int x, int y)
    {
        return (T *)((uint8_t *)raw_device_array + get_pixel_index(x, y));
    }

    __device__ inline const T *get_pixel(int x, int y) const
    {
        return (T *)((uint8_t *)raw_device_array + get_pixel_index(x, y));
    }

    __device__ __host__ int get_bytes_per_pixel() const
    {
        return bytes_per_pixel;
    }

    std::shared_ptr<T> get_device_array() const
    {
        return device_array;
    }


    size_t get_pitch() const
    {
        return pitch;
    }

private:
    int bytes_per_pixel = 0;
    std::shared_ptr<T> device_array;
    T* raw_device_array;
    size_t pitch = 0;

    friend class kernel::Managers::GPU::MemoryManager;
    friend class kernel::Container::Image;
    friend class kernel::Container::GPU::RGBAImage;

    ImageArray(size_t pitch_, const std::shared_ptr<T> &device_array_,
     int bytes_per_pixel_) : pitch(pitch_), device_array(device_array_), bytes_per_pixel(bytes_per_pixel_)
    {
        raw_device_array = device_array.get();
    }

    ImageArray()
    {
    }
};
}; // namespace GPU

}; // namespace Container

using GPUArray = Container::GPU::ImageArray<uint8_t>;

}; // namespace kernel
#endif