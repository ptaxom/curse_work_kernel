#ifndef KERNEL_CONTAINERS_GPU_RGBAIMAGE
#define KERNEL_CONTAINERS_GPU_RGBAIMAGE

#include <stdint.h>
#include "kernel/containers/GPU/ImageArray/ImageArray.h"
#include "kernel/managers/gpuMemoryManager/MemoryManager.h"
#include "kernel/Utility/Utility.h"
#include "kernel/containers/I_Image/I_Image.h"

namespace kernel
{

using RGBAArray = kernel::Container::GPU::ImageArray<int>;

namespace Container
{

namespace GPU
{

class RGBAImage : public kernel::Container::I_Image
{
public:
    RGBAImage(uint32_t width_, uint32_t height_, int channels = 0, std::shared_ptr<uint8_t> host_ptr = nullptr) : I_Image(width_, height_, 4)
    {
        image_storage = get_empty_shape(); 
        if (host_ptr.get())
        {
            switch (channels)
            {
            case 0:
                break; // Create empty image
            case 1:    // Create from gray scale
                init_from_grayscale(host_ptr);
                break;
            case 3:
                init_from_rgb(host_ptr);
                break;
            default:
                throw Utility::UnsupportedImageChannelFormatException("Cannot create RGBAImage from unsupported channels amount");
            }
        }
    }

    RGBAImage(const RGBAImage &other) : I_Image(other.width, other.height, 4)
    {
        using kernel::Managers::GPU::MemoryManager;
        image_storage = MemoryManager::allocate_pitched_ImageArray<int>(width, height, 1);
        MemoryManager::copy_ImageArray_to_ImageArray<int>(this->image_storage, (*const_cast<RGBAImage*>(&other)).image_storage, width, height, 1);
    }

    RGBAImage* get_ptr_copy() const
    {
        using kernel::Managers::GPU::MemoryManager;
        RGBAImage *other = new RGBAImage(width, height);
        MemoryManager::copy_ImageArray_to_ImageArray<int>(this->image_storage, other->image_storage, width, height, 1);
        return other;
    }


    void get_host_splitted_channels(uint8_t *rgb_host_ptr, uint8_t *alpha_host_ptr) const;

    RGBAArray get_image_storage() const
    {
        return image_storage;
    }

    void set_image_storage(RGBAArray image)
    {
        image_storage = image;
    }

    RGBAArray get_empty_shape() const
    {
        return kernel::Managers::GPU::MemoryManager::allocate_pitched_ImageArray<int>(width, height, 1);
    }

    void move_storage_from(const RGBAImage &other)
    {
        width = other.width;
        height = other.height;
        this->set_image_storage(other.image_storage);
    }

private:
    void init_from_grayscale(std::shared_ptr<uint8_t> host_ptr);

    void init_from_rgb(std::shared_ptr<uint8_t> host_ptr);

private:
    RGBAArray image_storage;
};

}; // namespace GPU

}; // namespace Container
}; // namespace kernel

#endif