#include "RGBAImage.h"

namespace kernel
{
namespace Container
{
namespace GPU
{

#define CONVERTER_BLOCK_SIZE 32

__global__ void gray_scale_cvt_kernel(const GPUArray src, RGBAArray out, const int width, const int height)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x < width && y < height)
    {
        int output_val = 0,
            input_val = src.get_pixel(x, y)[0];
        output_val = (input_val << 24) |
                     (input_val << 16) |
                     (input_val << 8) | 0xFF;

        out.get_pixel(x, y)[0] = output_val;
    }
}

__global__ void rgb_cvt_kernel(const GPUArray src, RGBAArray out, const int width, const int height)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x < width && y < height)
    {
        int output_val = (src.get_pixel(x, y)[0] << 24) |
                         (src.get_pixel(x, y)[1] << 16) |
                         (src.get_pixel(x, y)[2] << 8) | 0xFF;
        
        out.get_pixel(x,y)[0] = output_val;
    }
}

__global__ void rgba_split_kernel(const RGBAArray src, uint8_t *rgb, uint8_t *alpha, const int width, const int height)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x < width && y < height)
    {
        int pixel = *src.get_pixel(x, y);
        int rgb_index = 3 * (y * width + x),
            alpha_index = (y * width + x);
        rgb[rgb_index + 0] = (pixel >> 24) & 0xFF;
        rgb[rgb_index + 1] = (pixel >> 16) & 0xFF;
        rgb[rgb_index + 2] = (pixel >> 8) & 0xFF;
        alpha[alpha_index] = pixel & 0xFF;
    }
}

void RGBAImage::init_from_grayscale(std::shared_ptr<uint8_t> host_ptr)
{
    using kernel::Managers::GPU::MemoryManager;

    GPUArray source_array = MemoryManager::allocate_pitched_ImageArray<uint8_t>(width, height, 1);
    MemoryManager::copy_to_pitched_ImageArray(source_array, host_ptr, width, height, 1);


    dim3 block(CONVERTER_BLOCK_SIZE, CONVERTER_BLOCK_SIZE),
        grid((width + CONVERTER_BLOCK_SIZE - 1) / CONVERTER_BLOCK_SIZE,
             (height + CONVERTER_BLOCK_SIZE - 1) / CONVERTER_BLOCK_SIZE);

    gray_scale_cvt_kernel<<<grid, block>>>(source_array, image_storage, width, height);

    if (cudaDeviceSynchronize() != cudaSuccess)
        throw Utility::CUDAKernelException(
            cudaGetErrorName(cudaGetLastError()));
}

void RGBAImage::init_from_rgb(std::shared_ptr<uint8_t> host_ptr)
{
    
    using kernel::Managers::GPU::MemoryManager;

    GPUArray source_array = MemoryManager::allocate_pitched_ImageArray<uint8_t>(width, height, 3);
    MemoryManager::copy_to_pitched_ImageArray(source_array, host_ptr, width, height, 3);

    dim3 block(CONVERTER_BLOCK_SIZE, CONVERTER_BLOCK_SIZE),
        grid((width + CONVERTER_BLOCK_SIZE - 1) / CONVERTER_BLOCK_SIZE,
             (height + CONVERTER_BLOCK_SIZE - 1) / CONVERTER_BLOCK_SIZE);

    rgb_cvt_kernel<<<grid, block>>>(source_array, image_storage, width, height);


    if (cudaDeviceSynchronize() != cudaSuccess)
        throw Utility::CUDAKernelException(
            cudaGetErrorName(cudaGetLastError()));
}

void RGBAImage::get_host_splitted_channels(uint8_t *rgb_host_ptr, uint8_t *alpha_host_ptr) const
{
    using kernel::Managers::GPU::MemoryManager;

    if (rgb_host_ptr == nullptr && alpha_host_ptr == nullptr)
        return;

    size_t pixels_amount = width * height;
    std::shared_ptr<uint8_t> device_rgb = MemoryManager::allocate_raw_array<uint8_t>(pixels_amount * 3),
            device_alpha = MemoryManager::allocate_raw_array<uint8_t>(pixels_amount);

    dim3 block(CONVERTER_BLOCK_SIZE, CONVERTER_BLOCK_SIZE),
        grid((width + CONVERTER_BLOCK_SIZE - 1) / CONVERTER_BLOCK_SIZE,
             (height + CONVERTER_BLOCK_SIZE - 1) / CONVERTER_BLOCK_SIZE);

    rgba_split_kernel<<<grid, block>>>(image_storage, device_rgb.get(), device_alpha.get(), width, height);

    if (cudaDeviceSynchronize() != cudaSuccess)
        throw Utility::CUDAKernelException(
            cudaGetErrorName(cudaGetLastError()));

    if (rgb_host_ptr)
        MemoryManager::copy_raw_array_to_host(device_rgb, rgb_host_ptr, pixels_amount * 3);

    if (alpha_host_ptr)
        MemoryManager::copy_raw_array_to_host(device_alpha, alpha_host_ptr, pixels_amount);

}

}; // namespace GPU
}; // namespace Container
}; // namespace kernel