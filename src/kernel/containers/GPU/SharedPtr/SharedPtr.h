#ifndef KERNEL_CONTAINERS_GPU_SHARED_PTR
#define KERNEL_CONTAINERS_GPU_SHARED_PTR

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include <iostream>
#include <memory>

namespace kernel
{

namespace Container
{

namespace GPU
{

class SingletonCounter
{
    static SingletonCounter *instance;

    SingletonCounter()
    {
    }

public:
    static void allocate()
    {
        if (!instance)
            instance = new SingletonCounter();
        instance->allocates++;
    }

    static void deallocate()
    {
        if (!instance)
            instance = new SingletonCounter();
        instance->deallocates++;
    }

    static void print_stats()
    {

        std::cout << "Cuda Allocates: " << instance->allocates << std::endl;
        std::cout << "Cuda Deallocates: " << instance->deallocates << std::endl;
        std::cout << "Difference: " << (instance->allocates - instance->deallocates) << std::endl;
    }

private:
    size_t allocates = 0;
    size_t deallocates = 0;
};

template <typename T>
struct CudaDeleter
{
    void operator()(T *pointer)
    {
        if (pointer)
            cudaFree(pointer);
        pointer = nullptr;
        SingletonCounter::deallocate();
    }
};

template <class T>
std::shared_ptr<T> make_cuda_shared(T *pointer)
{
    return std::shared_ptr<T>(pointer, CudaDeleter<T>());
}

}; // namespace GPU
}; // namespace Container
}; // namespace kernel

#endif