#ifndef KERNEL_CONTAINER_IMAGE
#define KERNEL_CONTAINER_IMAGE

#include <stdio.h>
#include <cstring>
#include <string>
#include <iostream>
#include <memory>

#include "kernel/containers/GPU/ImageArray/ImageArray.h"
#include "kernel/containers/I_Image/I_Image.h"
#include "kernel/containers/GPU/RGBAImage/RGBAImage.h"

#include "kernel/managers/gpuMemoryManager/MemoryManager.h"
#include "kernel/Utility/Utility.h"

namespace kernel
{
namespace Container
{
class Image;

class ImageRow
{
public:
    const uint8_t *operator[](uint32_t col) const
    {
        return pixels + channels * (row_number * width + col);
    }

    uint8_t *operator[](uint32_t col)
    {
        return pixels + channels * (row_number * width + col);
    }

private:
    friend class Image;

    ImageRow(uint32_t row_number_, uint32_t width_, uint32_t channels_, uint8_t *pixels_) : row_number(row_number_),
                                                                                            channels(channels_),
                                                                                            pixels(pixels_),
                                                                                            width(width_)
    {
    }

    uint32_t row_number;
    uint32_t width;
    uint8_t channels;

    uint8_t *pixels;
};

class Image : public I_Image
{
    using PixelPtr = std::shared_ptr<uint8_t>;
public:
    Image() : I_Image(0,0,0)
    {
    }

    Image(int32_t width_, uint32_t height_, uint8_t channels_, uint8_t *pixels_) : I_Image(width_, height_, channels_), pixels(pixels_)
    {
    }

    Image(const Image &other) : I_Image(other.width, other.height, other.channels)
    {
        size_t image_size = width * height * channels * sizeof(uint8_t);
        uint8_t *new_pixels = new uint8_t[image_size];
        std::memcpy(new_pixels, other.pixels.get(), image_size);
        pixels.reset(new_pixels);
    }

    // Problems can be here
    Image &operator=(const Image &other)
    {

        width = other.width;
        height = other.height;
        channels = other.channels;
        size_t image_size = width * height * channels * sizeof(uint8_t);
        uint8_t *new_pixels = new uint8_t[image_size];
        std::memcpy(new_pixels, other.pixels.get(), image_size);
        pixels.reset(new_pixels);
        return *this;
    }

    // Problems can be here
    Image(const Image &&other) : I_Image(std::move(other))
    {
        std::cout << "Image(const Image &&other) !" << std::endl;
        width = std::move(other.width);
        height = std::move(other.height);
        channels = std::move(other.channels);
        pixels = std::move(other.pixels);
    }

    // Release memory resources of image
   

    bool operator==(const Image &other) const
    {
        if (pixels == nullptr)
            return false;
        if (channels == other.channels && width == other.width && height == other.height)
        {
            for (size_t i = 0; i < get_memory_size(); i++)
                if (pixels.get()[i] != other.pixels.get()[i])
                    return false;
            return true;
        }
        else
            return false;
    }

    PixelPtr get_pixels() const
    {
        return pixels;
    }

    size_t get_memory_size() const
    {
        return channels * width * height * sizeof(uint8_t);
    }

    ImageRow operator[](int row) const
    {
        return ImageRow(row, width, channels, pixels.get());
    }

    virtual bool is_empty() const override 
    {
        return pixels == nullptr || width == 0 || height == 0 || channels == 0;
    }

    operator bool() const
    {
        return !is_empty();
    }

    // Allocate pitched gpu array and copy image
    GPUArray get_pitched_image_array() const
    {
        using kernel::Managers::GPU::MemoryManager;
        GPUArray device_array = MemoryManager::allocate_pitched_ImageArray<uint8_t>(width, height, channels);
        MemoryManager::copy_to_pitched_ImageArray<uint8_t>(device_array, pixels,  width, height, channels);
        return device_array;
    }

    // Load from pithed GPU Array image
    void load_from_pitched_image_array(const GPUArray &source)
    {
        using kernel::Managers::GPU::MemoryManager;

        uint8_t *new_pixels = new uint8_t[get_memory_size()];        
        MemoryManager::load_pitched_ImageArray_to_raw_array<uint8_t>(source, pixels, width, height, channels);
        pixels.reset(new_pixels);
    }

    GPUArray create_empty_gpu_shape() const
    {
        using kernel::Managers::GPU::MemoryManager;
        GPUArray device_array = MemoryManager::allocate_pitched_ImageArray<uint8_t>(width, height, channels);
        return device_array;
    }

    kernel::Container::GPU::RGBAImage create_rgba_image() const
    {
        return kernel::Container::GPU::RGBAImage(width, height, channels, pixels);
    }

    void simple_gpu_test();

private:
    PixelPtr pixels;

};

}; // namespace Container
}; // namespace kernel

#endif