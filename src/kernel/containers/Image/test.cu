#include "Image.h"

namespace kernel
{
namespace Container
{

#define BLOCK_SIZE 32

__global__ void simpleKernel(const GPUArray src, GPUArray out, const int width, const int height)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x < width && y < height)
    {
        for (int channel = 0; channel < src.get_bytes_per_pixel(); channel++)
        {
            out.get_pixel(x, y)[channel] = src.get_pixel(x, y)[channel];
        }
        out.get_pixel(x, y)[0] = 0;
        out.get_pixel(x,y)[2] = 228;
    }
}

void Image::simple_gpu_test()
{
    GPUArray source_array = this->get_pitched_image_array();
    GPUArray dest_array = this->create_empty_gpu_shape();

    dim3 block(BLOCK_SIZE, BLOCK_SIZE),
        grid((width + BLOCK_SIZE - 1) / BLOCK_SIZE,
             (height + BLOCK_SIZE - 1) / BLOCK_SIZE);

    simpleKernel<<<grid, block>>>(source_array, dest_array, width, height);

    if (cudaDeviceSynchronize() != cudaSuccess)
        throw Utility::CUDAKernelException(
            cudaGetErrorName(cudaGetLastError()));

    this->load_from_pitched_image_array(dest_array);
}
}; // namespace Container
}; // namespace kernel