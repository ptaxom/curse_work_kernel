#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/time.h>
#include <pthread.h>

// #define SEND_DEBUG
// #define RECV_DEBUG

int connect_C(int port)
{
    int sockfd = -1;
    // create socket if it is not already created
    if (sockfd == -1)
    {
        // Create socket
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd == -1)
        {
            perror("Could not create socket");
            return -1;
        }
    }
    // set socket timeout
    struct timeval timeout;
    timeout.tv_sec = TIME_OUT_SECS;
    timeout.tv_usec = 0;

    if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
    {
        perror("setsockopt failed");
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
    {
        perror("setsockopt failed");
    }

    const char *address = "0.0.0.0";
    struct sockaddr_in server;

    // setup address structure
    if (inet_addr(address) == -1)
    {
        struct hostent *he;
        struct in_addr **addr_list;

        // resolve the hostname, its not an ip address
        if ((he = gethostbyname(address)) == NULL)
        {
            // gethostbyname failed
            herror("gethostbyname");

            return -1;
        }

        // Cast the h_addr_list to in_addr , since h_addr_list also has the ip address in long format
        // only
        addr_list = (struct in_addr **)he->h_addr_list;

        for (int i = 0; addr_list[i] != NULL; i++)
        {
            // strcpy(ip , inet_ntoa(*addr_list[i]) );
            server.sin_addr = *addr_list[i];
            break;
        }
    }

    // plain ip address
    else
    {
        server.sin_addr.s_addr = inet_addr(address);
    }

    server.sin_family = AF_INET;
    server.sin_port = htons(port);

    // Connect to remote server
    if (connect(sockfd, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return -1;
    }

    return sockfd;
}

struct Pair
{
    struct Package *request;
    struct Package *response;
    int socket;
    int *result;
};

int recv_request(Request *request, int socket)
{
    int actual_readed = recv(socket, request, sizeof(Request), 0);
    if (actual_readed < 0 || actual_readed != sizeof(Request))
        return -1;
}

int send_request(RequestTypes type, int info, int socket)
{
    Request *request = (Request *)calloc(sizeof(Request), 1);
    request->request_type = type;
    request->info = info;
    int actual_sended = send(socket, request, sizeof(Request), 0);
    free(request);
    if (actual_sended < 0 || actual_sended != sizeof(Request))
        return -1;
    else
        return 1;
}

#include <stdio.h>

int send_package(struct Package *to_send, int socket)
{
    int sended = 0,
        retries = MAX_RETRIES;
    char *buffer = to_send->data;
    Request *request = (Request *)calloc(sizeof(Request), 1);

    // Try to send Hello to server
    while (retries > 0)
    {
        if (send_request(REQUEST_HELLO, to_send->size, socket) < 0)
        {
            sleep(TIME_OUT_SECS);
            --retries;
        }
        else
            break;
    }
#ifdef SEND_DEBUG
    printf("Sended hello.\n");
#endif

    // Process main package
    while (sended < to_send->size && retries > 0)
    {
        int actual_size = to_send->size - sended < package_size ? to_send->size - sended : package_size;
        // Try to send server size of packet
        int request_result = send_request(REQUEST_INFO, actual_size, socket);
#ifdef SEND_DEBUG
        printf("Sended info request.\n");
#endif
        // Try to send server packet
        int actual_sended = send(socket, buffer + sended, actual_size, 0);
#ifdef SEND_DEBUG
        printf("Sended package = %d.\n", actual_sended);
#endif
        if (request_result < 0 || actual_sended < 0)
            goto error_label_send;
#ifdef SEND_DEBUG
        printf("Wait for request\n");
#endif
        // Wait server answer
        if (recv_request(request, socket) < 0 && errno != 0)
            goto error_label_send;
#ifdef SEND_DEBUG
        printf("Waited server answer OK = %s(%d).\n", request->request_type == RESPONSE_OK ? "true" : "false", request->request_type);
#endif
        // If error end transmitting
        if (request->request_type == RESPONSE_ERROR)
            goto error_label_send;
        // If server losed data make retry
        if (request->request_type == RESPONSE_RETRY)
        {
            --retries;
            continue;
        }
        if (request->request_type == RESPONSE_OK)
            sended += actual_sended;
        continue;

    error_label_send:
        if (errno != ETIME) // If not retry
        {
            printf("ERROR err: %d", errno);
            perror("str: ");
            free(request);
            return -1;
        }
        else
            --retries;
    }

    free(request);
    printf("Sending ended\n");
    return sended == to_send->size && retries > 0 ? 1 : -1;
}

int read_all(char *buffer, int target_size, int socket)
{
    int read_len = 0;

    while (target_size)
    {
        int received = recv(socket, buffer + read_len, target_size, 0);
        if (received <= 0)
            return received;

        read_len += received;
        target_size -= received;
    }

    return read_len;
}

int recv_package(struct Package *to_recv, int socket)
{
    int recieve_size = 228,
        retries = MAX_RETRIES;
    char *buffer = to_recv->data;
    to_recv->size = 0;
    Request *request = (Request *)calloc(sizeof(Request), 1);

    while (to_recv->size < recieve_size && retries > 0)
    {
        int status = 1;
#ifdef RECV_DEBUG
        printf("Entered in cycle.\n");
#endif
        if ((status = recv_request(request, socket)) >= 0) // If all right
        {
#ifdef RECV_DEBUG
            printf("Get response.\n");
#endif
            if (request->request_type == REQUEST_HELLO) // Started connection
            {
#ifdef RECV_DEBUG
                printf("HELLO with size = %d.\n", request->info);
#endif
                recieve_size = request->info;                 // Save package size
                to_recv->data = (char *)malloc(recieve_size); // Allocate place
                char *buffer = to_recv->data;
            }
            else if (request->request_type == REQUEST_INFO) // Processed main packet
            {
                int target_size = request->info;
#ifdef RECV_DEBUG
                printf("Prepare to read %d\n", target_size);
#endif
                int readed = read_all(to_recv->data + to_recv->size, target_size, socket);
                if (readed == target_size)
                {
#ifdef RECV_DEBUG
                    printf("Successful readed whole packet.\n");
#endif
                    to_recv->size += target_size;
                    send_request(RESPONSE_OK, 0, socket);
                }
                else if (readed < 0)
                {
#ifdef RECV_DEBUG
                    printf("ERROR %d %s\n", errno, strerror(errno));
#endif
                    send_request(RESPONSE_ERROR, 0, socket);
                    status = -1;
                }
                else
                {
#ifdef RECV_DEBUG
                    printf("Make retry.\n");
#endif
                    send_request(RESPONSE_RETRY, 0, socket);
                }
            }
            else
                status = -1;
        }
        else
            status = -1;
#ifdef RECV_DEBUG
        printf("Recv status: %d\n", status);
#endif
        if (status > 0)
            continue;

        // If error
        if (errno != ETIME)
        {
            printf("Recieving failed.\n");
            free(to_recv->data);
            free(request);
            return -1;
        }
        else
            --retries;
    }
    free(request);
    printf("Recieving successful.\n");
    return 1;
}

void *thread_request(void *data)
{
    struct Pair *pair = (struct Pair *)data;
    Request *request = (Request *)calloc(sizeof(Request), 1);
    int retries = MAX_RETRIES; // 3 tries to request
    while (retries > 0)
    {
        if (send_package(pair->request, pair->socket) < 0) // Send task to Python server
        {
            --retries;
            continue;
        }
        if (recv_request(request, pair->socket) < 0 || request->request_type != RESPONSE_DONE) // If not server done
        {
            --retries;
            continue;
        }
        if (recv_package(pair->response, pair->socket) < 0) // Response from server
        {
            --retries;
            continue;
        }
        else
            break;
    }
    int result = retries > 0 ? 1 : -1;
    *pair->result = result;

    free(request);
    free(pair->request);
    free(pair);
    pthread_exit(0);
}

int make_request_C(struct Package *request, struct Package *response, int *result, int socket)
{
    if (!request || !response)
        return -1;
    struct Pair *pair = (struct Pair *)calloc(sizeof(struct Pair), 1);
    pair->request = request;
    pair->response = response;
    pair->socket = socket;
    pair->result = result;
    pthread_attr_t attr;
    pthread_t thread_id;
    pthread_attr_init(&attr);
    if (pthread_create(&thread_id, &attr, thread_request, (void *)pair) < 0)
    {
        free(pair);
        return -1;
    }
    pthread_detach(thread_id);
    return 1;
}

void say_hello_C(int socket)
{
    send_request(REQUEST_HELLO, 0, socket);
}

struct Package *re;
int soc;

void *test_send(void *ptr)
{
    send_package(re, soc);
    pthread_exit(0);
}

int test_send_without_response(struct Package *request, int socket)
{
    re = request;
    soc = socket;
    pthread_attr_t attr;
    pthread_t thread_id;
    pthread_attr_init(&attr);
    if (pthread_create(&thread_id, &attr, test_send, (void *)NULL) < 0)
    {
        return -1;
    }
    pthread_detach(thread_id);
}