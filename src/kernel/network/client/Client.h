#ifndef KERNEL_NETWORK_CLIENT_CLIENT
#define KERNEL_NETWORK_CLIENT_CLIENT

#include <stdint.h>
#include "kernel/Utility/Utility.h"
#include <string.h>
#include "kernel/network/Protocol.h"
#include <malloc.h>
#include <iostream>

#include <sys/socket.h>
#include <unistd.h>

namespace kernel
{

namespace network
{
extern "C"
{
    int connect_C(int port);
    int make_request_C(struct Package *request, struct Package *response, int *result, int socket);
    void say_hello_C(int socket);
    int test_send_without_response(struct Package *request, int socket);

#include "Client.c"
};

class Client
{
public:
    Client(int port_) : port(port_)
    {
    }

    ~Client()
    {
        if (port > 0)
            close(port);
        if (last_response)
            free(last_response);
    }

    void connect()
    {
        socket_descriptor = connect_C(port);
        if (socket_descriptor < 0)
            throw kernel::Utility::ConnectionFailedException(std::string(strerror(errno)));
        last_response = (struct Package *)calloc(sizeof(struct Package), 1);
    }

    void say_hi() const
    {
        say_hello_C(socket_descriptor);
    }

    void test_send_str(std::string str) const
    {
        char *arr = (char *)malloc(str.length());
        memcpy(arr, str.c_str(), str.length());
        last_response->data = arr;
        last_response->size = str.length();
        test_send_without_response(last_response, socket_descriptor);
        free(arr);
    }

    void test_send_arr(char *ptr, int size) const
    {
        char *arr = (char *)malloc(size);
        memcpy(arr, ptr, size);
        last_response->data = arr;
        last_response->size = size;
        test_send_without_response(last_response, socket_descriptor);
        free(arr);
    }

    void make_request(char *ptr, int size)
    {
        is_success = 0;
        struct Package *request = (struct Package *)calloc(sizeof(struct Package), 1);
        request->data = ptr;
        request->size = size;
        make_request_C(request, last_response, &is_success, socket_descriptor);
    }

    int get_status() const
    {
        return is_success;
    }

    char *get_data() const
    {
        return last_response->data;
    }

    int get_data_size() const
    {
        return last_response->size;
    }

private:
    int port;
    int socket_descriptor = -1;
    int is_success = 0;

    struct Package *last_response;
};

}; // namespace network
}; // namespace kernel

#endif