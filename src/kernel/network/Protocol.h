#ifndef KERNEL_NETWORK_PROTOCOL
#define KERNEL_NETWORK_PROTOCOL

const int MAX_RETRIES = 5;
const int TIME_OUT_SECS = 5;

enum RequestTypes
{
    REQUEST_HELLO = 1,
    REQUEST_INFO = 2,
    RESPONSE_RETRY = 3,
    RESPONSE_OK = 4,
    RESPONSE_DONE = 5,
    RESPONSE_ERROR = 6,
    RESPONSE_CLOSE = 7
};

typedef struct request{
    int request_type;
    int info;
} Request;

const int package_size = 16 * 1024;

struct Package{
    int size;
    char *data;
};

#endif