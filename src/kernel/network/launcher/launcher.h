#ifndef KERNEL_NETWORK_LAUCNHER
#define KERNEL_NETWORK_LAUCNHER

#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <stdio.h>

#ifdef __cplusplus
extern "C"
{
#endif

    int launch_C(const char* command);


#ifdef __cplusplus
};
#endif

#include "launch.c"


int launch(unsigned short port)
{
    std::string shell_command("python3 /home/ptaxom/Desktop/term_work/src/python/main.py ");
    shell_command += std::to_string(port);
    return launch_C(shell_command.c_str());
}


#endif