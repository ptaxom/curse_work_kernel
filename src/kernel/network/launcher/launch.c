// #include "kernel/network/launcher/launcher.h"

int launch_C(const char* command)
{
    pid_t pid = fork();
    if (pid == 0)
    {
        int status = system(command);
        exit(0);
    }
    return pid;
}