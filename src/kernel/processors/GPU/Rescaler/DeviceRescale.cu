#include "BicubicRescaler.h"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include "kernel/managers/gpuMemoryManager/TextureManager.h"

namespace kernel
{
namespace Processors
{
namespace GPU
{

namespace CUDA_Bicubic_Rescale
{
#define BLOCK_SIZE 32
texture<int, 2, cudaReadModeElementType> rescale_tex;

float __device__ inline __attribute__((always_inline)) h00(float t)
{
    float tt = t * t;
    return 2.0 * tt * t - 3 * tt + 1;
}

float __device__ inline __attribute__((always_inline)) h10(float t)
{
    float tt = t * t;
    return tt * t - 2.0 * tt + t;
}

float __device__ inline __attribute__((always_inline)) h01(float t)
{
    float tt = t * t;
    return (-2.0 * t + 3) * tt;
}

float __device__ inline __attribute__((always_inline)) h11(float t)
{
    float tt = t * t;
    return (t - 1.0) * tt;
}

__device__ inline __attribute__((always_inline)) uint8_t saturate(float val)
{
    if (val < 0)
        return 0;
    if (val > 255)
        return 255;
    return (uint8_t)val;
}

uint8_t __device__ inline __attribute__((always_inline)) interpolate(float p0, float m0, float x, float p1, float m1)
{
    return saturate(h00(x) * p0 + h10(x) * m0 + h01(x) * p1 + h11(x) * m1);
}

__global__ void horizontal_pass(RGBAArray dest, const int new_width, const int height, const float x_ratio)
{
    int index_x = blockIdx.x * blockDim.x + threadIdx.x;
    int index_y = blockIdx.y * blockDim.y + threadIdx.y;
    if (index_x < new_width && index_y < height)
    {
        float x0 = __fdiv_rd(index_x, x_ratio) + 0.5;
        float x1 = x0 + 1.0;
        float x_1 = x0 - 1.0;
        float x2 = x1 + 1.0;
        float x = (float)index_x / x_ratio + 0.5 - x0;
        float y = index_y + 0.5;
        int px_1 = tex2D(rescale_tex, x_1, y),
            px0 = tex2D(rescale_tex, x0, y),
            px1 = tex2D(rescale_tex, x1, y),
            px2 = tex2D(rescale_tex, x2, y);
        int dest_pixel = 0;
        for (int i = 24; i >= 0; i -= 8)
        {
            uint8_t c_1 = (px_1 >> i) & 0xFF;
            uint8_t c0 = (px0 >> i) & 0xFF;
            uint8_t c1 = (px1 >> i) & 0xFF;
            uint8_t c2 = (px2 >> i) & 0xFF;
            float p0 = (float)c0,
                  p1 = (float)c1;
            float m0 = (p1 - (float)c_1) / 2.0,
                  m1 = ((float)c2 - p0) / 2.0;
            dest_pixel <<= 8;
            dest_pixel += interpolate(p0, m0, x, p1, m1);
        }
        dest.get_pixel(index_x, index_y)[0] = dest_pixel;
    }
}

__global__ void vertical_pass(RGBAArray dest, const int width, const int new_height, const float y_ratio)
{
    int index_x = blockIdx.x * blockDim.x + threadIdx.x;
    int index_y = blockIdx.y * blockDim.y + threadIdx.y;
    if (index_x < width && index_y < new_height)
    {
        float y0 = __fdiv_rd(index_y, y_ratio) + 0.5;
        float y1 = y0 + 1.0;
        float y_1 = y0 - 1.0;
        float y2 = y1 + 1.0;
        float y = (float)index_y / y_ratio + 0.5 - y0;
        float x = index_x + 0.5;
        int px_1 = tex2D(rescale_tex, x, y_1),
            px0 = tex2D(rescale_tex, x, y0),
            px1 = tex2D(rescale_tex, x, y1),
            px2 = tex2D(rescale_tex, x, y2);
        int dest_pixel = 0;
        for (int i = 24; i >= 0; i -= 8)
        {
            uint8_t c_1 = (px_1 >> i) & 0xFF;
            uint8_t c0 = (px0 >> i) & 0xFF;
            uint8_t c1 = (px1 >> i) & 0xFF;
            uint8_t c2 = (px2 >> i) & 0xFF;
            float p0 = (float)c0,
                  p1 = (float)c1;
            float m0 = (p1 - (float)c_1) / 2.0,
                  m1 = ((float)c2 - p0) / 2.0;
            dest_pixel <<= 8;
            dest_pixel += interpolate(p0, m0, y, p1, m1);
        }
        dest.get_pixel(index_x, index_y)[0] = dest_pixel;
    }
}

}; // namespace CUDA_Bicubic_Rescale

void BicubicRescaler::Process(RGBAImage &image) const
{
    size_t new_width = default_width;
    size_t new_height = default_height;
    if (is_rationed)
    {
        new_width = (float)image.get_width() * x_ratio;
        new_height = (float)image.get_height() * y_ratio;
    }
    else
    {
        x_ratio = (float)default_width / image.get_width();
        y_ratio = (float)default_height / image.get_height();
    }
    if (new_width != image.get_width())
    {
        horizontal_resize(image, new_width);
    }
    if (new_height != image.get_height())
    {
        vertical_resize(image, new_height);
    }
}

void BicubicRescaler::horizontal_resize(RGBAImage &image, size_t new_width) const
{
    using namespace CUDA_Bicubic_Rescale;
    using kernel::Managers::GPU::TextureManager;

    TextureManager::init_texture(rescale_tex, image, cudaAddressModeMirror);

    RGBAImage dest_image(new_width, image.get_height());

    RGBAArray out_array = dest_image.get_image_storage();
    uint32_t height = image.get_height();

    dim3 block(BLOCK_SIZE, BLOCK_SIZE),
        grid((new_width + BLOCK_SIZE - 1) / BLOCK_SIZE,
             (height + BLOCK_SIZE - 1) / BLOCK_SIZE);

    horizontal_pass<<<grid, block>>>(out_array, new_width, height, x_ratio);

    if (cudaDeviceSynchronize() != cudaSuccess)
        throw Utility::CUDAKernelException(
            cudaGetErrorName(cudaGetLastError()));

    if (cudaUnbindTexture(&rescale_tex) != cudaSuccess)
        throw Utility::CUDATextureException("Cannot unbind 2D array from texture");

    image.move_storage_from(dest_image);
}

void BicubicRescaler::vertical_resize(RGBAImage &image, size_t new_height) const
{
    using namespace CUDA_Bicubic_Rescale;
    using kernel::Managers::GPU::TextureManager;

    TextureManager::init_texture(rescale_tex, image, cudaAddressModeMirror);

    uint32_t width = image.get_width();
    RGBAImage dest_image(width, new_height);

    RGBAArray out_array = dest_image.get_image_storage();

    dim3 block(BLOCK_SIZE, BLOCK_SIZE),
        grid((width + BLOCK_SIZE - 1) / BLOCK_SIZE,
             (new_height + BLOCK_SIZE - 1) / BLOCK_SIZE);

    vertical_pass<<<grid, block>>>(out_array, width, new_height, y_ratio);

    if (cudaDeviceSynchronize() != cudaSuccess)
        throw Utility::CUDAKernelException(
            cudaGetErrorName(cudaGetLastError()));

    if (cudaUnbindTexture(&rescale_tex) != cudaSuccess)
        throw Utility::CUDATextureException("Cannot unbind 2D array from texture");

    image.move_storage_from(dest_image);
}

}; // namespace GPU
}; // namespace Processors
}; // namespace kernel