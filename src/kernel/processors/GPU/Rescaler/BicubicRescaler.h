#ifndef KERNEL_PROCESSORS_GPU_BICUBICSCALER
#define KERNEL_PROCESSORS_GPU_BICUBICSCALER

#include "kernel/processors/GPU/IProcessor.h"

namespace kernel
{
namespace Processors
{
namespace GPU
{

class BicubicRescaler : public IProcessor
{
public:
    explicit BicubicRescaler(float x_ratio_ , float y_ratio_ ) : y_ratio(y_ratio_), x_ratio(x_ratio_), is_rationed(true)
    {}

    virtual void Process(RGBAImage &image) const override;

    void as_pixels(int width, int height)
    {
        default_width = width;
        default_height = height;
        is_rationed = false;
    }

    private:
        void horizontal_resize(RGBAImage &image, size_t new_width) const;
        void vertical_resize(RGBAImage &image, size_t new_height) const;

private:
    mutable float y_ratio = 1.0;
    mutable float x_ratio = 1.0;

    uint32_t default_width = 100;
    uint32_t default_height = 100;

    bool is_rationed = true;
};

}; // namespace GPU
}; // namespace Processors
}; // namespace kernel

#endif