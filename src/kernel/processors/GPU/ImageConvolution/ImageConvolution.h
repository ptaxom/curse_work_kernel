#ifndef KERNEL_PROCESSORS_GPU_IMAGECONVOLUTION
#define KERNEL_PROCESSORS_GPU_IMAGECONVOLUTION

#include "kernel/processors/GPU/IProcessor.h"
#include "kernel/containers/ConvMatrix/ConvolutionMatrix.h"

namespace kernel
{
namespace Processors
{
namespace GPU
{

template <typename DType>
class ImageConvolution : public IProcessor
{
    using ConvKernel = kernel::Container::ConvolutionMatrix<DType>;
    using deviceConvKernel = kernel::Container::GPU::ImageArray<DType>;
    const int BLOCK_SIZE = 32;

public:
    ImageConvolution(const ConvKernel &conv_kernel_) : IProcessor(), conv_kernel(conv_kernel_) {}

    virtual void Process(RGBAImage &image) const override;

private:
    ConvKernel conv_kernel;
};

}; // namespace GPU

}; // namespace Processors
}; // namespace kernel

#endif