#include "ImageConvolution.h"
#include "kernel/managers/gpuMemoryManager/TextureManager.h"

namespace kernel
{
namespace Processors
{
namespace GPU
{

namespace CUDA_Kernel_Conv
{
#define BLOCK_SIZE 32
texture<int, 2, cudaReadModeElementType> conv_tex;

template <typename T>
using deviceConvKernel = kernel::Container::GPU::ImageArray<T>;

template <typename DType>
__device__ uint8_t saturate(DType val)
{
    if (val < 0)
        return 0;
    if (val > 255)
        return 255;
    return (uint8_t)val;
}

template <typename DType>
__global__ void conv_simple(RGBAArray out, const deviceConvKernel<DType> kernel, const DType norm_coef, const int radius, const int width, const int height)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x < width && y < height)
    {
        DType r0 = 0,
              g0 = 0,
              b0 = 0,
              a0 = 0;
        int local_radius = radius;
        for (int ry = -local_radius; ry <= local_radius; ry++)
        {
            for (int rx = -local_radius; rx <= local_radius; rx++)
            {
                DType multiplyer = kernel.get_pixel(local_radius + rx, local_radius + ry)[0];
                int value = tex2D(conv_tex, x + rx + 0.5, y + ry + 0.5);

                r0 += multiplyer * ((value >> 24) & 0xFF);
                g0 += multiplyer * ((value >> 16) & 0xFF);
                b0 += multiplyer * ((value >> 8) & 0xFF);
                a0 += multiplyer * (value & 0xFF);
            }
        }
        // Instruction level parallelism
        r0 /= norm_coef;
        g0 /= norm_coef;
        b0 /= norm_coef;
        a0 /= norm_coef;

        uint8_t r = saturate(r0);
        uint8_t g = saturate(g0);
        uint8_t b = saturate(b0);
        uint8_t a = saturate(a0);

        int out_value = (r << 24) |
                        (g << 16) |
                        (b << 8) | a;

        out.get_pixel(x, y)[0] = out_value;
    }
}

template <typename DType>
__global__ void conv_shared(RGBAArray out, const deviceConvKernel<DType> kernel, const DType norm_coef, const int radius, const int width, const int height)
{
    int tile_X = blockIdx.x * (blockDim.x - 2 * radius),
        tile_Y = blockIdx.y * (blockDim.y - 2 * radius);
    int global_X = tile_X + threadIdx.x - radius,
        global_Y = tile_Y + threadIdx.y - radius;
    int tid_X = threadIdx.x,
        tid_Y = threadIdx.y,
        conv_size = 2 * radius + 1;
    extern __shared__ int *cached_image;
    DType *shared_kernel = (DType *)(cached_image + blockDim.x * blockDim.y);

    cached_image[tid_Y * blockDim.x + tid_X] = tex2D(conv_tex, global_X + 0.5, global_Y + 0.5);
    __syncthreads();

    if (tid_X < conv_size && tid_Y < conv_size)
        shared_kernel[tid_Y * conv_size + tid_X] = kernel.get_pixel(tid_X, tid_Y)[0];
    __syncthreads();

    if (tid_X < radius || tid_Y < radius || tid_X >= blockDim.x - radius || tid_Y >= blockDim.y || global_X >= width || global_Y >= height)
        return;

    DType r0 = 0,
          g0 = 0,
          b0 = 0,
          a0 = 0;
    for (int ry = -radius; ry <= radius; ry++)
    {
        for (int rx = -radius; rx <= radius; rx++)
        {
            DType multiplyer = shared_kernel[(radius + ry) * conv_size + (radius + ry)];
            int value = cached_image[(tid_Y + ry) * blockDim.x + tid_X + rx];

            r0 += multiplyer * ((value >> 24) & 0xFF);
            g0 += multiplyer * ((value >> 16) & 0xFF);
            b0 += multiplyer * ((value >> 8) & 0xFF);
            a0 += multiplyer * (value & 0xFF);
        }
    }
    // Instruction level parallelism
    r0 /= norm_coef;
    g0 /= norm_coef;
    b0 /= norm_coef;
    a0 /= norm_coef;

    uint8_t r = saturate(r0);
    uint8_t g = saturate(g0);
    uint8_t b = saturate(b0);
    uint8_t a = saturate(a0);

    int out_value = (r << 24) |
                    (g << 16) |
                    (b << 8) | a;

    out.get_pixel(global_X, global_Y)[0] = out_value;
}

}; // namespace CUDA_Kernel_Conv

// Maybe width in bytes (CUDA BIND TEXTURE)

#define Process(DType)                                                                                                        \
    template <>                                                                                                               \
    void ImageConvolution<DType>::Process(RGBAImage &image) const                                                             \
    {                                                                                                                         \
        using namespace CUDA_Kernel_Conv;                                                                                     \
        using kernel::Managers::GPU::TextureManager;                                                                          \
                                                                                                                              \
        TextureManager::init_texture(conv_tex, image, cudaAddressModeMirror);                                                 \
        deviceConvKernel device_kernel = conv_kernel.get_gpu_array();                                                         \
        RGBAArray out_array = image.get_empty_shape();                                                                        \
        int radius = conv_kernel.get_conv_size() / 2,                                                                         \
            width = image.get_width(),                                                                                        \
            height = image.get_height();                                                                                      \
        DType norm_coef = conv_kernel.get_coefficient();                                                                      \
        dim3 block(BLOCK_SIZE, BLOCK_SIZE);                                                                                   \
        if (radius < 15)                                                                                                      \
        {                                                                                                                     \
            int efficient_block_size = BLOCK_SIZE - 2 * radius;                                                               \
            dim3 grid((width + efficient_block_size - 1) / BLOCK_SIZE,                                                        \
                      (height + BLOCK_SIZE - 1) / BLOCK_SIZE);                                                                \
            size_t conv_size = conv_kernel.get_conv_size();                                                                   \
            size_t shared_mem_size = BLOCK_SIZE * BLOCK_SIZE * sizeof(int) + conv_size * conv_size * sizeof(DType);           \
            conv_simple<DType><<<grid, block, shared_mem_size>>>(out_array, device_kernel, norm_coef, radius, width, height); \
        }                                                                                                                     \
        else                                                                                                                  \
        {                                                                                                                     \
            dim3 grid((width + BLOCK_SIZE - 1) / BLOCK_SIZE,                                                                  \
                      (height + BLOCK_SIZE - 1) / BLOCK_SIZE);                                                                \
            conv_simple<DType><<<grid, block>>>(out_array, device_kernel, norm_coef, radius, width, height);                  \
        }                                                                                                                     \
                                                                                                                              \
        if (cudaDeviceSynchronize() != cudaSuccess)                                                                           \
            throw Utility::CUDAKernelException(                                                                               \
                cudaGetErrorName(cudaGetLastError()));                                                                        \
                                                                                                                              \
        if (cudaUnbindTexture(&conv_tex) != cudaSuccess)                                                                      \
            throw Utility::CUDATextureException("Cannot unbind 2D array from texture");                                       \
        image.set_image_storage(out_array);                                                                                   \
    }

Process(double)
Process(int)
Process(long)
Process(float)
Process(char)

}; // namespace GPU
}; // namespace Processors
}; // namespace kernel