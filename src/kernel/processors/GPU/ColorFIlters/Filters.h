#ifndef KERNEL_PROCESSORS_GPU_COLORFILTERS
#define KERNEL_PROCESSORS_GPU_COLORFILTERS

#include "kernel/processors/GPU/IProcessor.h"

namespace kernel
{
namespace Processors
{
namespace GPU
{

class GammaTransform : public IProcessor
{
public:
    explicit GammaTransform(float gamma_) : gamma(gamma_)
    {}

    virtual void Process(RGBAImage &image) const override;
    
    private:
        float gamma;
};


class Negative : public IProcessor
{
public:
    explicit Negative()
    {}

    virtual void Process(RGBAImage &image) const override;
};

class GrayScale : public IProcessor
{
public:
    explicit GrayScale()
    {}

    virtual void Process(RGBAImage &image) const override;
};



};
};
};

#endif