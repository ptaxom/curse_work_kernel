#include "Filters.h"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <cmath>
#include "kernel/managers/gpuMemoryManager/TextureManager.h"
#include <algorithm>

namespace kernel
{
namespace Processors
{
namespace GPU
{

namespace CUDA_Filters
{

#define BLOCK_SIZE 32
texture<int, 2, cudaReadModeElementType> filter_tex;

__device__ inline __attribute__((always_inline)) uint8_t saturate(float val)
{
    if (val < 0)
        return 0;
    if (val > 255)
        return 255;
    return (uint8_t)val;
}

uint8_t __device__ inline __attribute__((always_inline)) gamma_transform(uint8_t px, const float gamma)
{
    float pw = log2f(px) * gamma;
    return saturate(powf(2.0, pw));
}

__global__ void gamma(RGBAArray dest, const int new_width, const int new_height, const float gamma)
{
    int index_x = blockIdx.x * blockDim.x + threadIdx.x;
    int index_y = blockIdx.y * blockDim.y + threadIdx.y;
    if (index_x < new_width && index_y < new_height)
    {
        int pixel = tex2D(filter_tex, index_x + 0.5, index_y + 0.5);
        int n_pixel = gamma_transform(pixel >> 24, gamma) << 24 |
                      gamma_transform((pixel >> 16) & 0xFF, gamma) << 16 |
                      gamma_transform((pixel >> 8) & 0xFF, gamma) << 8 |
                      0xFF;
        dest.get_pixel(index_x, index_y)[0] = n_pixel;
    }
}

__global__ void gsc(RGBAArray dest, const int new_width, const int new_height)
{
    int index_x = blockIdx.x * blockDim.x + threadIdx.x;
    int index_y = blockIdx.y * blockDim.y + threadIdx.y;
    if (index_x < new_width && index_y < new_height)
    {
        int pixel = tex2D(filter_tex, index_x + 0.5, index_y + 0.5);
        int r = pixel >> 24,
            g = (pixel >> 16) & 0xFF,
            b = (pixel >> 8) & 0xFF;

        double gray_val = 0.144 *r + 0.587 *g + 0.299 *b;
        r = g = b = saturate(gray_val);
        
        dest.get_pixel(index_x, index_y)[0] = r << 24 | g << 16 | b << 8 | 0xFF;
    }
}

__global__ void neg(RGBAArray dest, const int new_width, const int new_height)
{
    int index_x = blockIdx.x * blockDim.x + threadIdx.x;
    int index_y = blockIdx.y * blockDim.y + threadIdx.y;
    if (index_x < new_width && index_y < new_height)
    {
        int pixel = tex2D(filter_tex, index_x + 0.5, index_y + 0.5);

        dest.get_pixel(index_x, index_y)[0] = ~pixel | 0xFF;
    }
}

enum KernelType
{
    GS,
    NG,
    GT
};

void launch_kernel(RGBAImage &image, KernelType type, double arg = 1.0)
{
    using kernel::Managers::GPU::TextureManager;

    TextureManager::init_texture(filter_tex, image, cudaAddressModeMirror);

    RGBAArray out_array = image.get_empty_shape();
    uint32_t width = image.get_width(),
             height = image.get_height();

    dim3 block(BLOCK_SIZE, BLOCK_SIZE),
        grid((width + BLOCK_SIZE - 1) / BLOCK_SIZE,
             (height + BLOCK_SIZE - 1) / BLOCK_SIZE);

    if (type == GT && arg != 1.0)
        gamma<<<grid, block>>>(out_array, width, height, arg);
    if (type == GS)
        gsc<<<grid, block>>>(out_array, width, height);
    if (type == NG)
        neg<<<grid, block>>>(out_array, width, height);

    if (cudaDeviceSynchronize() != cudaSuccess)
        throw Utility::CUDAKernelException(
            cudaGetErrorName(cudaGetLastError()));

    if (cudaUnbindTexture(&filter_tex) != cudaSuccess)
        throw Utility::CUDATextureException("Cannot unbind 2D array from texture");

    image.set_image_storage(out_array);
}

}; // namespace CUDA_Filters

void GammaTransform::Process(RGBAImage &image) const
{
    using namespace CUDA_Filters;
    launch_kernel(image, GT, gamma);
}

void GrayScale::Process(RGBAImage &image) const
{
    using namespace CUDA_Filters;
    launch_kernel(image, GS);
}

void Negative::Process(RGBAImage &image) const
{
    using namespace CUDA_Filters;
    launch_kernel(image, NG);
}

}; // namespace GPU
}; // namespace Processors
}; // namespace kernel