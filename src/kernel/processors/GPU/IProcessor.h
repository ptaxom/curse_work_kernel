#ifndef KERNEL_PROCESSORS_GPU_IPROCESSOR
#define KERNEL_PROCESSORS_GPU_IPROCESSOR

#include "kernel/containers/GPU/RGBAImage/RGBAImage.h"

namespace kernel
{
using kernel::Container::GPU::RGBAImage;

namespace Processors
{
namespace GPU
{

class IProcessor
{
public:
    IProcessor() {}

    virtual ~IProcessor() {}

    virtual void Process(RGBAImage &image) const = 0;
};

}; // namespace GPU
}; // namespace Processors
}; // namespace kernel

#endif