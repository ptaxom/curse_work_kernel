#include "Rotator.h"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <cmath>
#include "kernel/managers/gpuMemoryManager/TextureManager.h"
#include <algorithm>

namespace kernel
{
namespace Processors
{
namespace GPU
{

namespace CUDA_Rotate
{

#define BLOCK_SIZE 32
texture<int, 2, cudaReadModeElementType> image_tex;

__global__ void rotate(RGBAArray dest, const int new_width, const int new_height, const float theta, const float normal, const float half_normal)
{
    int index_x = blockIdx.x * blockDim.x + threadIdx.x;
    int index_y = blockIdx.y * blockDim.y + threadIdx.y;
    if (index_x < new_width && index_y < new_height)
    {
        float u = ((float)index_x - (float)new_width / 2 + 0.5) / (float)new_width;
        float v = ((float)index_y - (float)new_height / 2 + 0.5) / (float)new_height;

        float tu = u * cosf(-theta) - v * sinf(-theta);
        float tv = v * cosf(-theta) + u * sinf(-theta);

        float normal_x = (tu + half_normal) / normal,
              normal_y = (tv + half_normal) / normal;

        int pixel = tex2D(image_tex, normal_x, normal_y);
        dest.get_pixel(index_x, index_y)[0] = pixel;
    }
}

}; // namespace CUDA_Rotate

void Rotator::Process(RGBAImage &image) const
{
    using namespace CUDA_Rotate;
    using namespace std;
    using kernel::Managers::GPU::TextureManager;

    TextureManager::init_texture(image_tex, image, cudaAddressModeBorder, true);

    uint32_t height = image.get_height(),
             width = image.get_width();

    double angle = atan2(height, width);
    const double pi4 = acos(-1) / 4.0;
    const double radius = (sqrt(height * height + width * width) + 1 )/ 2;
    uint32_t new_width = 0,
             new_height = 0;
    for (int i = 0; i < 4; i++)
    {
        size_t buf = 2 * radius * abs(cos(angle + theta + pi4 * i));
        if (buf > new_width)
            new_width = buf;
        buf = 2 * radius * abs(sin(angle + theta + pi4 * i));
        if (buf > new_height)
            new_height = buf;
    }
    float normal = abs(sin(theta)) + abs(cos(theta));


    RGBAImage dest_image(new_width, new_height);

    RGBAArray out_array = dest_image.get_image_storage();

    dim3 block(BLOCK_SIZE, BLOCK_SIZE),
        grid((new_width + BLOCK_SIZE - 1) / BLOCK_SIZE,
             (new_height + BLOCK_SIZE - 1) / BLOCK_SIZE);

    rotate<<<grid, block>>>(out_array, new_width, new_height, theta, normal, normal / 2.0);

    if (cudaDeviceSynchronize() != cudaSuccess)
        throw Utility::CUDAKernelException(
            cudaGetErrorName(cudaGetLastError()));

    if (cudaUnbindTexture(&image_tex) != cudaSuccess)
        throw Utility::CUDATextureException("Cannot unbind 2D array from texture");

    image.move_storage_from(dest_image);
}

}; // namespace GPU
}; // namespace Processors
}; // namespace kernel