#ifndef KERNEL_PROCESSORS_GPU_ROTATOR
#define KERNEL_PROCESSORS_GPU_ROTATOR

#include "kernel/processors/GPU/IProcessor.h"

namespace kernel
{
namespace Processors
{
namespace GPU
{

class Rotator : public IProcessor
{
public:
    explicit Rotator(float theta_) : theta(theta_)
    {}

    virtual void Process(RGBAImage &image) const override;
    
    private:
        float theta;
};
}; // namespace GPU
}; // namespace Processors
}; // namespace kernel

#endif