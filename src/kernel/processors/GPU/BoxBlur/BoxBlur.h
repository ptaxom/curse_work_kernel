#ifndef KERNEL_PROCESSORS_GPU_BOXBLUR
#define KERNEL_PROCESSORS_GPU_BOXBLUR

#include "kernel/processors/GPU/IProcessor.h"

namespace kernel
{
namespace Processors
{
namespace GPU
{

class BoxBlur : public IProcessor
{
public:
    BoxBlur(int radius_ = 1) : IProcessor(), radius(radius_) {}

    virtual void Process(RGBAImage &image) const override
    {
        wrapper(image);
    }

private:
    void wrapper(RGBAImage &image) const;

    int radius = 1;
};

}; // namespace GPU
}; // namespace Processors
}; // namespace kernel

#endif