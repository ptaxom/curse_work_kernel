#include "BoxBlur.h"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include "kernel/managers/gpuMemoryManager/TextureManager.h"

namespace kernel
{
namespace Processors
{
namespace GPU
{

namespace CUDA_Box_Blur{

#define BLOCK_SIZE 32
texture<int, 2, cudaReadModeElementType> box_blur_tex;

__global__ void box_blur(RGBAArray out, const int radius, const int width, const int height)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    if (x < width && y < height)
    {
        int normalize = (2 * radius + 1) * (2 * radius + 1),
            r0 = 0,
            g0 = 0,
            b0 = 0,
            a0 = 0;
        for (int ry = -radius; ry <= radius; ry++)
            for (int rx = -radius; rx <= radius; rx++)
            {
                int value = tex2D(box_blur_tex, x + rx + 0.5, y + ry + 0.5);

                r0 += (value >> 24) & 0xFF;
                g0 += (value >> 16) & 0xFF;
                b0 += (value >> 8) & 0xFF;
                a0 += value & 0xFF;
            }
        r0 /= normalize;
        g0 /= normalize;
        b0 /= normalize;
        a0 /= normalize;
        int out_value = (r0 << 24) |
                        (g0 << 16) |
                        (b0 << 8) | a0;
        out.get_pixel(x, y)[0] = out_value;
    }
}

};

void BoxBlur::wrapper(RGBAImage &image) const
{
    using namespace CUDA_Box_Blur;
    using kernel::Managers::GPU::TextureManager;

    TextureManager::init_texture(box_blur_tex, image, cudaAddressModeMirror);

    RGBAArray out_array = image.get_empty_shape();
    uint32_t width = image.get_width(),
             height = image.get_height();

    dim3 block(BLOCK_SIZE, BLOCK_SIZE),
        grid((width + BLOCK_SIZE - 1) / BLOCK_SIZE,
             (height + BLOCK_SIZE - 1) / BLOCK_SIZE);
 
    box_blur<<<grid, block>>>(out_array, radius, width, height);

    if (cudaDeviceSynchronize() != cudaSuccess)
        throw Utility::CUDAKernelException(
            cudaGetErrorName(cudaGetLastError()));

    if (cudaUnbindTexture(&box_blur_tex) != cudaSuccess)
        throw Utility::CUDATextureException("Cannot unbind 2D array from texture");

    image.set_image_storage(out_array);
}

}; // namespace GPU
}; // namespace Processors
}; // namespace kernel