#ifndef KERNEL_UTILITY_HIST
#define KERNEL_UTILITY_HIST

#include <vector>
#include <fstream>
#include "kernel/containers/GPU/RGBAImage/RGBAImage.h"

namespace kernel
{

namespace Utility
{

class Histogram
{
    using vector = std::vector<double>;
public:
    static Histogram build_gpu_histogram(const kernel::Container::GPU::RGBAImage &image);
    static Histogram build_cpu_histogram(const kernel::Container::GPU::RGBAImage &image);

    void serialize(std::string file_name) const
    {
        std::fstream file(file_name, std::ios::out);
        for(int i = 0; i < 256; i++)
            file << red_channel[i] << " ";
        file << std::endl;
        for(int i = 0; i < 256; i++)
            file << green_channel[i] << " ";
        file << std::endl;
        for(int i = 0; i < 256; i++)
            file << blue_channel[i] << " ";
        file << std::endl;
        for(int i = 0; i < 256; i++)
            file << alpha_channel[i] << " ";
        file << std::endl;
        file.close();
    }

private:
    Histogram(const uint32_t *red_hist, const uint32_t *green_hist, const uint32_t *blue_hist, const uint32_t *alpha_hist)
    {
        init_channel(red_channel, red_hist);
        init_channel(green_channel, green_hist);
        init_channel(blue_channel, blue_hist);
        init_channel(alpha_channel, alpha_hist);
    }

    void init_channel(vector &channel, const uint32_t *channel_ptr)
    {
        size_t accumulator = 0;
        channel.resize(256);
        for (int i = 0; i < 256; i++)
            accumulator += channel_ptr[i];

        for (int i = 0; i < 256; i++)
            channel[i] = 1. * channel_ptr[i] / (double)accumulator;
    }

private:
    vector red_channel;
    vector green_channel;
    vector blue_channel;
    vector alpha_channel;
};

}; // namespace Utility
}; // namespace kernel

#endif
