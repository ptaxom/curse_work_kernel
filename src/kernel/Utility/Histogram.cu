#include "Histogram.h"

#include "kernel/managers/gpuMemoryManager/MemoryManager.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <device_functions.h>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <pthread.h>

namespace kernel
{

namespace Utility
{

namespace GPU
{
#define BLOCK_SIZE 32

__global__ void shared_histogramm(const kernel::RGBAArray src, unsigned int *dest, const int width, const int height)
{
    int x = blockIdx.x * blockDim.x + threadIdx.x;
    int y = blockIdx.y * blockDim.y + threadIdx.y;
    int index = threadIdx.y * 32 + threadIdx.x;
    __shared__ uint32_t particle_hist[4 * 256];
    particle_hist[index] = 0;
    __syncthreads();
    if (x < width && y < height)
    {
        int pixel = src.get_pixel(x, y)[0];
        int r = pixel >> 24 & 0xFF;
        int g = (pixel >> 16) & 0xFF;
        int b = (pixel >> 8) & 0xFF;
        int a = pixel & 0xFF;
        atomicAdd(&particle_hist[r], 1);
        atomicAdd(&particle_hist[256 + g], 1);
        atomicAdd(&particle_hist[512 + b], 1);
        atomicAdd(&particle_hist[768 + a], 1);
    }
    __syncthreads();
    atomicAdd(dest + index, particle_hist[index]);
}
}; // namespace GPU

Histogram Histogram::build_gpu_histogram(const kernel::Container::GPU::RGBAImage &image)
{
    using kernel::Managers::GPU::MemoryManager;

    int width = image.get_width(),
        height = image.get_height();

    kernel::RGBAArray rgba_array = image.get_image_storage();
    auto device_hist = MemoryManager::allocate_raw_array<uint32_t>(256 * 4);

    dim3 block(BLOCK_SIZE, BLOCK_SIZE),
        grid((width + BLOCK_SIZE - 1) / BLOCK_SIZE,
             (height + BLOCK_SIZE - 1) / BLOCK_SIZE);

    GPU::shared_histogramm<<<grid, block>>>(rgba_array, device_hist.get(), width, height);

    if (cudaDeviceSynchronize() != cudaSuccess)
        throw Utility::CUDAKernelException(
            cudaGetErrorName(cudaGetLastError()));
 
    std::shared_ptr<uint32_t> host_hist(new uint32_t[256 * 4]);

    MemoryManager::copy_raw_array_to_host<uint32_t>(device_hist, host_hist, 256 * 4);

    Histogram histogram(host_hist.get(), host_hist.get() + 256,
                        host_hist.get() + 256 * 2, host_hist.get() + 256 * 3);
    return histogram;
}

namespace thread_worker
{

struct ParticleHistogram
{
    ParticleHistogram(size_t w, size_t h, int c, uint8_t *i_p, uint32_t *c_p) : width(w), height(h), channel(c), channel_ptr(c_p), image_ptr(i_p)
    {}

    size_t width;
    size_t height;
    int channel;
    uint8_t *image_ptr;
    uint32_t *channel_ptr;
};

void *thread_hist_builder(void *data)
{
    ParticleHistogram *hist = static_cast<ParticleHistogram*>(data);
    int channel = hist->channel;
    for(int y = 0; y < hist->height; y++)
        for(int x = 0; x < hist->width; x++)
            {
                uint8_t intensity = hist->image_ptr[3 * (y * hist->width + x) + channel];
                ++hist->channel_ptr[intensity];
            }
    delete hist;
    pthread_exit(0);
}

}; // namespace thread_worker

Histogram Histogram::build_cpu_histogram(const kernel::Container::GPU::RGBAImage &image)
{

    size_t width = image.get_width(),
           height = image.get_height();
    size_t mem_size = width * height;
    uint8_t *rgb = (uint8_t*)malloc(mem_size * 3),
            *alpha = (uint8_t*)malloc(mem_size);
    uint32_t *hist = (uint32_t*)calloc(1024 , sizeof(uint32_t));

    image.get_host_splitted_channels(rgb, alpha);

    std::vector<pthread_t> threads(3, 0);
    for(int i = 0; i < 3; i++)
    {
        using namespace thread_worker;
        ParticleHistogram *task = new ParticleHistogram(width, height, i, rgb, hist + 256 * i);
        pthread_attr_t attr;
        pthread_t thread_id;
        pthread_attr_init(&attr);
        int status = pthread_create(&thread_id, &attr, thread_hist_builder, static_cast<void *>(task));
        threads[i] = thread_id;
    }
    for(int y = 0; y < height; y++)
        for(int x = 0; x < width; x++)
        {
            uint8_t alp = alpha[y * width + x];
            ++hist[256 * 3 + alp];
        }
            
    
    for (const auto &id : threads)
        pthread_join(id, NULL);
    
    Histogram histogram(hist, hist + 256,
                        hist + 256 * 2, hist + 256 * 3);
    free(hist);
    free(rgb);
    free(alpha);
    return histogram; 
}

}; // namespace Utility
}; // namespace kernel