#ifndef KERNEL_UTILITY
#define KERNEL_UTILITY

#include <string>
#include <iostream>
#include <stdexcept>

namespace kernel
{

namespace Utility
{

// CUDA Exceptions

class CUDAMemcpyException : public std::runtime_error
{
public:
    CUDAMemcpyException(std::string what = "") : std::runtime_error(what)
    {
    }
};

class CUDAMallocException : public std::runtime_error
{
public:
    CUDAMallocException(std::string what = "") : std::runtime_error(what)
    {
    }
};

class CUDAKernelException : public std::runtime_error
{
public:
    CUDAKernelException(std::string what = "") : std::runtime_error(what)
    {
    }
};

class CUDATextureException : public std::runtime_error
{
public:
    CUDATextureException(std::string what = "") : std::runtime_error(what)
    {
    }
};


// Container exceptions

class EmptyImageException : public std::runtime_error
{
public:
    EmptyImageException(std::string what = "") : std::runtime_error(what)
    {
    }
};

class EmptyRawArrayException : public std::runtime_error
{
public:
    EmptyRawArrayException(std::string what = "") : std::runtime_error(what)
    {
    }
};

class UnsupportedImageChannelFormatException : public std::runtime_error
{
public:
    UnsupportedImageChannelFormatException(std::string what = "") : std::runtime_error(what)
    {
    }
};

class ContainerDimensionsException : public std::runtime_error
{
public:
    ContainerDimensionsException(std::string what = "") : std::runtime_error(what)
    {
    }
};


// Network exceptions

class ConnectionFailedException : public std::runtime_error
{
public:
    ConnectionFailedException(std::string what = "") : std::runtime_error(what)
    {
    }
};

}; // namespace Utility
}; // namespace kernel

#endif