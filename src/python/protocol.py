


class PROTOCOL:
    PACKAGE_SIZE = 16 * 1024    # Default packet size
    HEADER_SIZE = 8             # Read 8 bytes for header
    MAX_RETRIES = 5             # Maximum 5 retries for send
    TIME_OUT_SECS = 10          # 5 seconds for timeout

    REQUEST_HELLO = 1
    REQUEST_INFO = 2
    RESPONSE_RETRY = 3
    RESPONSE_OK = 4
    RESPONSE_DONE = 5
    RESPONSE_ERROR = 6
    RESPONSE_CLOSE = 7