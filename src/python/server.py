from client import Client
import os
import select
import socket
import sys
from protocol import PROTOCOL

class Server:
    """
    Simple TCP Server for service C++/CUDA App requests for ML image analize
    """
    server_instances = []
    child_pids = []

    def __init__(self, port, max_clients, *args, **kwargs):
        """
        Init server by port
        """
        self.port = port
        self.max_clients = max_clients
        
        # Create socket
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Prepare socket. He will use port if busy, hosted on 0.0.0.0:port and handle max_clients clients and timeout 5 secs for recv
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind(("0.0.0.0", port))
        self.server_socket.listen(max_clients)
        self.server_socket.settimeout(PROTOCOL.TIME_OUT_SECS)

        self.inputs = [self.server_socket]
        self.pids = []

        Server.server_instances.append(self)
        print("Server started.")


    def handle_new_connection(self):
        """
            Create new process, which process new connection
            Increase perfomance by avoid GIL
        """
        connection, client_address = self.server_socket.accept()
        pid = os.fork()
        if pid > 0: 
            Server.child_pids.append(pid)
            return

        client = Client(connection)
        client.start()


    def start(self):
        """
            Start server executing and listening
        """
        self.active = True
        while self.active and self.inputs:
            readable, writable, exceptional = select.select(self.inputs, [] , self.inputs, PROTOCOL.TIME_OUT_SECS)
            for issued_socket in readable:
                if issued_socket is self.server_socket:
                    print("New connection")
                    self.handle_new_connection()
            # if len(readable) == 0:
            #     print("timeout...")
                