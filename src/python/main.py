#!/usr/bin/python

import sys
import os, signal
from server import Server

def sigterm_handler(signum, frame):
        """
        Handling signal and shut down all servers
        """
        print("Server terminated by interrupt")
        for server in Server.server_instances:  #Disable all loops
            server.active = False
            for child_pid in server.child_pids:
                os.kill(child_pid, signal.SIGKILL)
        sys.exit(0)
        # Kill all clients
        # self_pid = os.getppid()
        # group_pid = os.getpgid(self_pid)
        # os.killpg(group_pid, signal.SIGKILL)
            




if __name__ == "__main__":
    signal.signal(signal.SIGTERM, sigterm_handler)
    signal.signal(signal.SIGINT, sigterm_handler)
    args = sys.argv
    # server = Server(port = int(args[1]), max_clients = 5)
    server = Server(port = 8888, max_clients = 5)
    server.start()