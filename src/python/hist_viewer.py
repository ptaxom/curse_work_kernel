#!/usr/bin/python
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from numpy import arange
import sys
import cv2

def plot_hist(channels, title):
    labels = ['Red channel', 'Green channel', 'Blue channel', 'Alpha channel']
    colors = ['r', 'g', 'b', 'y']
    y_locator = MaxNLocator(nbins=2)
        
    x_axis = arange(256)
    f, axarr = plt.subplots(2, 2)
    f.canvas.set_window_title(title)
    for y in range(0, 2):
        for x in range(0, 2):
            axarr[y, x].yaxis.set_minor_locator(MaxNLocator(nbins=1))
            axarr[y, x].yaxis.set_major_locator(y_locator)

    for i, channel in enumerate(channels):
        y, x = i // 2, i % 2
        axarr[y, x].plot(x_axis, channel, color = colors[i])
        axarr[y, x].set_title(labels[i])

    plt.setp([a.get_xticklabels() for a in axarr[0, :]], visible=False)
    plt.setp([a.get_yticklabels() for a in axarr[0, :]], visible=False)
    plt.setp([a.get_yticklabels() for a in axarr[:, 0]], visible=False)


def build_hist(path, *args):
    try:

        hist_file = open(path, "r")
        channels = []
        for i in range(0, 4):
            values = hist_file.readline().split(" ")[:-1]
            channels.append(values)

        plot_hist(channels, path)

    except IOError:
        print("Cannot open file.")
    

def cv_hist(path):
    img = cv2.imread('build/processed.jpeg')
    color = ('b','g','r')
    channels = []
    for i,col in enumerate(color):
        histr = cv2.calcHist([img], [i], None,[256],[0,256])
        channels.insert(0, histr)
    plot_hist(channels, "CV")


if __name__ == "__main__":
    args = sys.argv
    if len(args) == 2:
        build_hist("build/hist_cpu.txt")
        build_hist("build/hist_gpu.txt")
        cv_hist(args[1])
        plt.show()
    else:
        print("Usage hist_viewer.py [path]")
