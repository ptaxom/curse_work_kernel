import sys
import socket
from protocol import PROTOCOL
from PIL import Image
import time
import errno

class Client:
    """
        Class for handle single connection
    """

    def __init__(self, connection, *args, **kwargs):
        self.connection = connection
        
    
    def close(self):
        self.active = False
        print("Closed connection with {socket_desc}.".format(socket_desc=self.connection.fileno()))
        self.connection.close()
        sys.exit(0)

    def send_request(self, req_type , req_info):
        request = req_type.to_bytes(4, byteorder = 'little') + req_info.to_bytes(4, byteorder = 'little')
        # print('request: ', request, req_type, req_info)
        return self.connection.send(request)

    @staticmethod
    def parse_header(data):
        req_type = int.from_bytes(data[0:4], "little") 
        req_info = int.from_bytes(data[4:8], "little") 
        return (req_type, req_info)


    def start(self):
        self.active = True

        while self.active:
            data = self.connection.recv(PROTOCOL.HEADER_SIZE)
            if len(data) == 0:
                self.close()
            elif len(data) != PROTOCOL.HEADER_SIZE:
                self.send_request(PROTOCOL.RESPONSE_ERROR, 0)
            else:
                req_type , req_info = Client.parse_header(data)
                if req_type == PROTOCOL.REQUEST_HELLO:
                    self.last_package_size = req_info
                    recved = self.recv_package()
                    # print ('Differse: ', len(recved) - 224 * 224 * 3)
                    image = Image.frombytes("RGB", (224, 224), recved)
                    image.show()
                    out_image = bytes()
                    for x in recved:
                        out_image += int(255 - x).to_bytes(1, 'little')                    
                    self.send_request(PROTOCOL.RESPONSE_DONE, 0)
                    self.send_package(out_image)                    

        self.close()
    
    
    def read_all(self, target_size):
        total_recieved = bytes()
        while target_size > 0:
            received = self.connection.recv(target_size)
            total_recieved += received
            
            if len(received) == 0:
                self.active = False         # Client closed connection
                # print('package ended')
                return total_recieved

            target_size -= len(received)

        # print('package full')
        return total_recieved

    def recv_package(self):
        target_package_size = self.last_package_size
        tries = PROTOCOL.MAX_RETRIES
        package = bytes()

        # print('Pre in cycle', target_package_size, tries, self.active)
        while target_package_size > 0 and tries > 0 and self.active:
            header = self.connection.recv(PROTOCOL.HEADER_SIZE)
            # print('Entered in cycle', target_package_size, tries, self.active)
            if len(header) == 0:
                self.active = False
                return package

            req_type , want_to_send = Client.parse_header(header)
            # print('Header: ', req_type, want_to_send)
            if req_type == PROTOCOL.REQUEST_INFO:
                try:
                    last_data = self.read_all(want_to_send)
                    # print('recved: ', len(last_data))
                    if len(last_data) == want_to_send or not self.active:
                        # print('All right')
                        package += last_data
                        target_package_size -= len(last_data)
                        self.send_request(PROTOCOL.RESPONSE_OK, 0)
                    else:
                        # print('retry')
                        self.send_request(PROTOCOL.RESPONSE_RETRY, 0)
                except Exception:
                    # print('Exception ')
                    tries -= 1
                    self.send_request(PROTOCOL.RESPONSE_ERROR, 0)
            else:
                tries -= 1
        if tries > 0:
            return package
        else:
            raise RuntimeError('Transmition failed.')

    def send_package(self, package_to_send):
        # print('Sending')
        total_sended, retries = 0, PROTOCOL.MAX_RETRIES
        # Try to greetings with client
        while retries > 0:
            status = self.send_request(PROTOCOL.REQUEST_HELLO, len(package_to_send))
            if status <= 0 or status != PROTOCOL.HEADER_SIZE:
                retries -= 1
                time.sleep(PROTOCOL.TIME_OUT_SECS)
            else:
                break
        
        while total_sended < len(package_to_send) and retries > 0:
            packet_to_send = package_to_send[total_sended : total_sended + PROTOCOL.PACKAGE_SIZE]
        
            status = self.send_request(PROTOCOL.REQUEST_INFO, len(packet_to_send)) # Notify server about sendin
            if status <= 0:
                raise RuntimeError('Transmition failed. Cannot make request.')
        
            status = self.connection.send(packet_to_send)   # Send packet

            header = self.connection.recv(PROTOCOL.HEADER_SIZE) # Waiting for response
            if len(header) != PROTOCOL.HEADER_SIZE:
                raise RuntimeError('Transmition failed. Cannot wait for response.')

            req_type , want_to_send = Client.parse_header(header)
            if req_type == PROTOCOL.RESPONSE_ERROR:
                raise RuntimeError('Transmition failed. Client declined transmition.')
            
            if req_type == PROTOCOL.RESPONSE_RETRY:
                retries -= 1
                continue
            
            if req_type == PROTOCOL.RESPONSE_OK:
                total_sended += len(packet_to_send)

        if retries <= 0:
            raise RuntimeError('Transmition failed. Out of retries.')
