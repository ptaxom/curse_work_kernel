#include "configuration.h"

#ifdef KERNEL

#include <iostream>
#include "kernel/network/client/Client.h"
#include <chrono>
#include "kernel/containers/Image/Image.h";
#include "kernel/processors/GPU/BoxBlur/BoxBlur.h"
#include "kernel/Utility/Utility.h"
#include "kernel/processors/GPU/ColorFIlters/Filters.h"

int main()
{
    {
        using namespace std;
        cout << "Box blur." << std::endl;
        using namespace kernel::Processors;
        using namespace kernel::Container;
        using namespace kernel::Utility;
        int width = 512;
        int height = 512;
        uint8_t *ptr = new uint8_t[width * height * 3];
        Image img(width, height, 3, ptr);
        kernel::Container::GPU::RGBAImage image = img.create_rgba_image();
        for (int i = 1; i < 33; i++)
        {
            kernel::Processors::GPU::BoxBlur bb(i);
            auto start = std::chrono::steady_clock::now();
            bb.Process(image);
            auto end = std::chrono::steady_clock::now();
            cout << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " ";
        }
    }
    {
        using namespace std;
        cout << endl << "Negative." << std::endl;
        using namespace kernel::Processors;
        using namespace kernel::Container;
        using namespace kernel::Utility;
        for (int i = 1; i < 33; i++)
        {
            int width = 256 * i;
            int height = 256 * i;
            uint8_t *ptr = new uint8_t[width * height * 3];
            Image img(width, height, 3, ptr);
            kernel::Container::GPU::RGBAImage image = img.create_rgba_image();
            kernel::Processors::GPU::Negative ng;
            auto start = std::chrono::steady_clock::now();
            ng.Process(image);
            auto end = std::chrono::steady_clock::now();
            cout << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " ";
        }
    }

    return 0;
}

#endif

#ifdef GUI

#include <wx/wx.h>

#include "GUI/MyFrame/MyFrame.h"

class MyApp : public wxApp
{
public:
    virtual bool OnInit() override
    {
        if (!wxApp::OnInit())
            return false;

        MyFrame *frame = new MyFrame("Curse work");

        frame->Show(true);

        return true;
    }
};

wxIMPLEMENT_APP(MyApp);

#endif