#include "Utility.h"

#include "kernel/processors/GPU/Rescaler/BicubicRescaler.h"
#include "GUI/UI_Parameters/UI_Constants.h"

namespace Utility
{

using kernel::Container::Image;

// Create full copy of wxImage ignored Alpha channel
Image create_from_wx_image(const wxImage &img)
{
    wxImage image = img;
    int width = image.GetWidth();
    int height = image.GetHeight();
    size_t image_size = width * height * 3 * sizeof(uint8_t);
    uint8_t *pixels = (uint8_t *)malloc(image_size);
    std::memcpy(pixels, image.GetData(), image_size);
    return Image(width, height, 3, pixels);
}

wxImage to_wx_image(const Image &other)
{
    Image memory_container = Image(other);
    return wxImage(memory_container.get_width(), memory_container.get_height(), memory_container.get_pixels().get(), false);
}

Image Image_from_RGBAImage(const RGBAImage &other)
{
    size_t pixels_cnt = other.get_width() * other.get_height();
    uint8_t *rgb_ptr = nullptr, *alpha_ptr = nullptr;

    rgb_ptr = (uint8_t *)malloc(pixels_cnt * 3);

    other.get_host_splitted_channels(rgb_ptr, nullptr);

    return Image(other.get_width(), other.get_height(), 3, rgb_ptr);
}

wxImage wxImage_from_RGBAImage(const RGBAImage &other)
{
    size_t pixels_cnt = other.get_width() * other.get_height();
    uint8_t *rgb_ptr = nullptr, *alpha_ptr = nullptr;

    rgb_ptr = (uint8_t *)malloc(pixels_cnt * 3);
    alpha_ptr = (uint8_t *)malloc(pixels_cnt);

    other.get_host_splitted_channels(rgb_ptr, alpha_ptr);

    return wxImage(other.get_width(), other.get_height(), rgb_ptr, alpha_ptr);
}

wxBitmap to_wx_bitmap(const Image &other)
{
    return wxBitmap(to_wx_image(other));
}

Image load(const std::string &path)
{
    wxBitmap bitmap(path);
    return create_from_wx_image(bitmap.ConvertToImage());
}

Image load(const wxString &path)
{
    wxBitmap bitmap(path);
    return create_from_wx_image(bitmap.ConvertToImage());
}

void save(const Image &img, const std::string &path, wxBitmapType type)
{

    to_wx_bitmap(img).SaveFile(path, type);
}

void fit_to_screen_size(RGBAImage &image)
{
    using namespace UI_PARAMS::Constants;

    if (image.get_width() > IMAGE_MAX_WIDTH ||
        image.get_height() > IMAGE_MAX_HEIGHT)
    {
        double x_ratio = (double)image.get_width() / IMAGE_MAX_WIDTH,
               y_ratio = (double)image.get_height() / IMAGE_MAX_HEIGHT,
               ratio;
        ratio = x_ratio > y_ratio ? 1.0 / x_ratio : 1.0 / y_ratio;
        kernel::Processors::GPU::BicubicRescaler scaler(ratio, ratio);
        scaler.Process(image);
    }
}

}; // namespace Utility