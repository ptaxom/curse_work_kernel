#ifndef GUI_UTILITY
#define GUI_UTILITY

#include "kernel/containers/Image/Image.h"
#include "kernel/containers/GPU/RGBAImage/RGBAImage.h"

#include "wx/wx.h"

namespace Utility
{

using kernel::Container::Image;
using kernel::Container::GPU::RGBAImage;

// Create full copy of wxImage ignored Alpha channel
Image create_from_wx_image(const wxImage &image);

wxImage to_wx_image(const Image& other);

wxImage wxImage_from_RGBAImage(const RGBAImage& other);

Image Image_from_RGBAImage(const RGBAImage& other);

wxBitmap to_wx_bitmap(const Image& other);

Image load(const std::string &path);

Image load(const wxString &path);

void save(const Image &img, const std::string &path, wxBitmapType type);

void fit_to_screen_size(RGBAImage &other);

}; // namespace Utility

#endif