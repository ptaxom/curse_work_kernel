#ifndef GUI_OBSERVABLE_TASK_EXECUTOR
#define GUI_OBSERVABLE_TASK_EXECUTOR

#include <functional>
#include <wx/wx.h>
#include <set>

class TaskExecutor : public wxThread
{
public:
    TaskExecutor(const std::function<void(void)> &task_) : wxThread(wxTHREAD_DETACHED), task(task_) {}

    virtual ExitCode Entry()
    {
        declare_thread(this);
        task();
        undeclare_thread(this);
        return (ExitCode)0;
    }

    static void destroy_all()
    {
        set_guarder.Enter();
        for (TaskExecutor *thread : threads)
            thread->Delete();
        threads.clear();
        set_guarder.Leave();
    }

private:
    static void declare_thread(TaskExecutor *executor)
    {
        set_guarder.Enter();
        threads.insert(executor);
        set_guarder.Leave();
    }

     static void undeclare_thread(TaskExecutor *executor)
    {
        set_guarder.Enter();
        if (threads.count(executor) == 1)
            threads.erase(executor);
        set_guarder.Leave();
    }

    static std::set<TaskExecutor *> threads;
    static wxCriticalSection set_guarder;

private:
    std::function<void(void)> task;
};

#endif