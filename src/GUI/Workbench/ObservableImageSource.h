#ifndef GUI_OBSERVABLE_IMAGE_SOURCE
#define GUI_OBSERVABLE_IMAGE_SOURCE

#include "kernel/containers/GPU/RGBAImage/RGBAImage.h"
#include <wx/wx.h>

class ObservableImageSource
{
    using RGBAImage = kernel::Container::GPU::RGBAImage;

public:
    ObservableImageSource() {}

    virtual ~ObservableImageSource() {}

    virtual wxImage get_wxImage() const = 0;
    
    virtual RGBAImage *get_RGBAImage() const = 0;

    virtual bool empty() const = 0;

    virtual void notify_all() const = 0;

    virtual void add_observer(wxEvtHandler*) = 0;
};

#endif