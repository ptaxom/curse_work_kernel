#ifndef SRC_GUI_WORKBENCH_H
#define SRC_GUI_WORKBENCH_H

#include <vector>
#include <set>
#include <wx/wx.h>
#include <stdexcept>
#include <thread>

#include "kernel/containers/GPU/RGBAImage/RGBAImage.h"
#include "kernel/processors/GPU/IProcessor.h"

#include "GUI/Utility/Utility.h"
#include "GUI/Workbench/ObservableImageSource.h"
#include "TaskExecutor.h"

wxDEFINE_EVENT(OK_EVENT, wxCommandEvent);

class Workbench : public ObservableImageSource
{
    using RGBAImage = kernel::Container::GPU::RGBAImage;
    using IProcessor = kernel::Processors::GPU::IProcessor;

public:
    Workbench(int cache_deep_) : ObservableImageSource(), cache_deep(cache_deep_), current_cache_pos(0)
    {
    }

    void load(const wxString &path)
    {
        // clear previous workbench state
        clear();

        // Load CPU image
        kernel::Container::Image host_image = Utility::load(path);
        // Convert to GPU Image
        cached_image = host_image.create_rgba_image();
        RGBAImage *device_image = cached_image.get_ptr_copy();
        // If image greater then max size, scale down
        Utility::fit_to_screen_size(*device_image);
        add_to_cache(device_image);

        notify_all();
    }

    // By default save as JPEG
    void save(const wxString &path_to_save, wxBitmapType type = wxBITMAP_TYPE_JPEG) const
    {
        if (cache.empty())
            throw std::runtime_error("Image cache is empty!");
        
        int filters_cnt = processors.size() - cache.size() + current_cache_pos + 1;
        const Workbench *pointer = this;
        auto task = [pointer, path_to_save, type, filters_cnt]() {
            RGBAImage *device_image = pointer->cached_image.get_ptr_copy();
            for(int i = 0; i < filters_cnt; i++)
                pointer->processors[i]->Process(*device_image);

            wxImage image = Utility::wxImage_from_RGBAImage(*device_image);
            wxBitmap bitmap(image);
            bitmap.SaveFile(path_to_save, type);
            delete device_image;
            pointer->on_ok("Succeseful saved.");
        };

        TaskExecutor *executor = new TaskExecutor(task);
        executor->Run();
    }

    void add_processor(IProcessor *processor)
    {
        if (cache.empty())
            throw std::runtime_error("Image cache is empty!");

        // Make copy of last cache
        RGBAImage *new_image = cache[current_cache_pos]->get_ptr_copy();
        add_to_cache(new_image);
        processors.push_back(processor);

        Workbench *pointer = this;
        auto task = [pointer]() {
            IProcessor *processor = pointer->processors.back();
            RGBAImage *image = pointer->cache.back();
            processor->Process(*image);
            pointer->notify_all();
            pointer->on_ok("Succesefuly processed.");
        };

        TaskExecutor *executor = new TaskExecutor(task);
        executor->Run();
    }

    void undo()
    {
        if (current_cache_pos == 0)
            throw std::runtime_error("Cannot undo.");

        --current_cache_pos;
        notify_all();
    }

    void redo()
    {
        if (current_cache_pos + 1 >= cache.size())
            throw std::runtime_error("Cannot redo.");

        ++current_cache_pos;
        notify_all();
    }

    virtual wxImage get_wxImage() const override
    {
        if (cache.empty())
            throw std::runtime_error("Image cache is empty!");

        return Utility::wxImage_from_RGBAImage(*cache[current_cache_pos]);
    }

    virtual RGBAImage *get_RGBAImage() const override
    {
        if (cache.empty())
            throw std::runtime_error("Image cache is empty!");

        return cache[current_cache_pos];
    }

    virtual bool empty() const override
    {
        return cache.empty();
    }

    ~Workbench()
    {
        clear();
    }

    virtual void add_observer(wxEvtHandler *observer) override
    {
        if (observer)
            observers.insert(observer);
    }

    virtual void notify_all() const override
    {
        wxCommandEvent event(wxEVT_PAINT);
        for (const wxEvtHandler *obs : observers)
        {
            wxEvtHandler *observer = const_cast<wxEvtHandler *>(obs);
            wxPostEvent(observer, event);
        }
    }

    void set_ok_handler(wxEvtHandler *ok_handler_)
    {
        this->ok_handler = ok_handler_;
    }

    void on_ok(wxString message) const
    {
        if (ok_handler)
        {
            wxCommandEvent event(OK_EVENT);
            event.SetString(message);
            wxPostEvent(const_cast<wxEvtHandler *>(ok_handler), event);
        }
    }

private:
    void clear()
    {
        clear_ptr_vector(cache);
        clear_ptr_vector(processors);
        current_cache_pos = 0;
    }

    template <typename T>
    void clear_ptr_vector(std::vector<T> &vector)
    {
        for (T pointer : vector)
            delete pointer;
        vector.clear();
    }

    void add_to_cache(RGBAImage *image)
    {
        if (current_cache_pos + 1 != cache.size())
        {
            int delta = cache.size() - current_cache_pos - 1;
            for (int i = 0; i < delta; i++)
            {
                delete cache.back();
                delete processors.back();
                cache.pop_back();
                processors.pop_back();
            }
        }
        if (cache.size() == cache_deep)
        {
            delete cache.front();
            cache.erase(cache.begin());
            cache.push_back(image);
        }
        else
        {
            cache.push_back(image);
            current_cache_pos = cache.size() - 1;
        }
    }

private:
    std::set<wxEvtHandler *> observers;

    RGBAImage cached_image = RGBAImage(1, 1);

    int cache_deep;
    int current_cache_pos;
    std::vector<RGBAImage *> cache;
    std::vector<IProcessor *> processors;
    wxEvtHandler *ok_handler = nullptr;
};

#endif