#ifndef GUI_MY_FRAME
#define GUI_MY_FRAME

#include <wx/wx.h>

#include "configuration.h"

#include "GUI/MyPanel/MyPanel.h"
#include "GUI/UI_Parameters/UI_Constants.h"
#include "GUI/Utility/Utility.h"
#include "GUI/Workbench/Workbench.h"
#include "GUI/MyDialog/FilterAdder.h"

#include "kernel/containers/Image/Image.h"
#include "kernel/containers/GPU/SharedPtr/SharedPtr.h"

#include <cuda_runtime_api.h>

class MyFrame : public wxFrame
{
  public:
    MyFrame(const wxString &title);

    ~MyFrame()
    {
      cudaDeviceReset();
      TaskExecutor::destroy_all();
      // kernel::Container::GPU::SingletonCounter::print_stats();
      delete workbench;
    }

    void OnQuit(wxCommandEvent &event);
    void OnAbout(wxCommandEvent &event);
    void OnOpen(wxCommandEvent &event);
    void OnSave(wxCommandEvent &event);
    void OnOk(wxCommandEvent &event);

    void refresh();

  private:
    void OnBoxBlur(wxCommandEvent &event);

    void OnNegative(wxCommandEvent &event);
    void OnGrayScale(wxCommandEvent &event);
    void OnGamma(wxCommandEvent &event);

    void OnRotation(wxCommandEvent &event);
    void OnScale(wxCommandEvent &event);

    void OnUndo(wxCommandEvent &event);
    void OnRedo(wxCommandEvent &event);

    // void OnQuit(wxCommandEvent &event);
  private:
    MyPanel *panel;
    Workbench *workbench;

    wxDECLARE_EVENT_TABLE();
};

enum
{
    Minimal_Quit = wxID_EXIT,

    Minimal_About = wxID_ABOUT,
    
    File_Choose = 228,

    Filter_Menu = 229,

    Conv_Filter_SubMenu = 230,
    Box_Blur_Filter = 231,
    Gaussian_Blur_Filter = 232,

    Color_Filter_SubMenu = 240,
    GrayScale_Filter = 241,
    Negative_Filter = 242,
    Gamma_Filter = 243,
    WeighedChannels_Filter = 244,

    Rotate_Filter = 250,
    Rescale_Filter = 251,

    Ctrl_Z = 260,
    Ctrl_Y = 261,

    Save = 270
    


};

#endif