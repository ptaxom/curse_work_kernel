#include "MyFrame.h"

MyFrame::MyFrame(const wxString &title)
    : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, UI_PARAMS::Constants::DEFAULT_WINDOW_SIZE)
{
    workbench = new Workbench(5);

    wxMenu *fileMenu = new wxMenu;

    wxMenu *helpMenu = new wxMenu;
    helpMenu->Append(Minimal_About, "&About\tF1", "Show about dialog");

    fileMenu->Append(Minimal_Quit, "E&xit\tAlt-X", "Quit this program");
    fileMenu->Append(File_Choose, "&Open\tAlt-O", "Open file for edit");
    fileMenu->Append(Save, "&Save\tAlt-S", "Save file");
    fileMenu->Append(Ctrl_Z, "&Undo\tAlt-U", "Undo action");
    fileMenu->Append(Ctrl_Y, "&Redo\tAlt-R", "Redo action");

    wxMenu *filterMenu = new wxMenu;

    wxMenu *confFilter = new wxMenu;
    confFilter->Append(Box_Blur_Filter, "Box blur");

    wxMenu *colorFilter = new wxMenu;
    colorFilter->Append(Negative_Filter, "Negative");
    colorFilter->Append(GrayScale_Filter, "Grayscale");
    colorFilter->AppendSeparator();
    colorFilter->Append(Gamma_Filter, "Gamma transform");

    filterMenu->AppendSubMenu(confFilter, wxT("Convolution filters"));
    filterMenu->AppendSeparator();
    filterMenu->AppendSubMenu(colorFilter, wxT("Color filters"));
    filterMenu->AppendSeparator();

    filterMenu->Append(Rotate_Filter, "Rotation");
    filterMenu->Append(Rescale_Filter, "Rescale");

    wxMenuBar *menuBar = new wxMenuBar();
    menuBar->Append(fileMenu, "&File");
    menuBar->Append(filterMenu, "F&ilters");
    menuBar->Append(helpMenu, "&Help");

    SetMenuBar(menuBar);

    CreateStatusBar(2);

    wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);
    this->panel = new MyPanel((wxFrame *)this, workbench);
    sizer->Add(panel, 1, wxEXPAND);
    this->SetSizer(sizer);
    this->SetAutoLayout(true);

    SetStatusText("Choose file");

    workbench->add_observer(panel);
    workbench->set_ok_handler(this);

    this->SetMinSize(UI_PARAMS::Constants::DEFAULT_WINDOW_SIZE);
}

void MyFrame::OnQuit(wxCommandEvent &WXUNUSED(event))
{
    cudaDeviceReset();
    TaskExecutor::destroy_all();
    Close(true);
}

void MyFrame::OnAbout(wxCommandEvent &WXUNUSED(event))
{
    wxMessageBox(wxString::Format("Term work"),
                 "Info",
                 wxOK | wxICON_INFORMATION,
                 this);
}

void MyFrame::refresh()
{
    this->panel->Refresh();
}

#include "kernel/processors/GPU/BoxBlur/BoxBlur.h"
#include "kernel/processors/GPU/Rotator/Rotator.h"

void MyFrame::OnOpen(wxCommandEvent &WXUNUSED(event))
{
    wxString path_to_file;
#ifndef MYDBG
    wxFileDialog openFileDialog(this, _("Open image"), "", "",
                                "Image files (*.bmp;*.jpg;*.jpeg;*.png)|*.bmp;*.jpg;*.jpeg;*.png", wxFD_OPEN | wxFD_FILE_MUST_EXIST);

    if (openFileDialog.ShowModal() == wxID_CANCEL)
        return;
    path_to_file = openFileDialog.GetPath();
#else
    path_to_file = "/home/ptaxom/Desktop/SPOVM/lab3/resources/Huge.jpg";
#endif
    workbench->load(path_to_file);

    panel->Refresh();

    wxString str = path_to_file.substr(1 + path_to_file.find_last_of('/'));
    SetStatusText(str);
}

#include "kernel/processors/GPU/BoxBlur/BoxBlur.h"

void MyFrame::OnBoxBlur(wxCommandEvent &event)
{
    using kernel::Processors::GPU::BoxBlur;
    if (workbench->empty())
        wxMessageBox(wxString("Open image first!"),
                     "Info",
                     wxOK | wxICON_INFORMATION,
                     NULL);
    else
    {
        CustomScroll cs1(1, 50, "Box radius", [](int x) { return 2 * x + 1; }, [](int x) { return std::string("Box radius: ") + std::to_string(2 * x + 1); });
        Workbench *wb = this->workbench;
        auto callback = [wb](std::vector<double> args) {
            try
            {
                wb->add_processor(new BoxBlur((int)args[0]));
            }
            catch (const std::runtime_error &ex)
            {
                wxMessageBox(wxString(ex.what()),
                             "Info",
                             wxOK | wxICON_INFORMATION,
                             NULL);
            }
        };
        FilterAdder *custom = new FilterAdder(wxT("Add box blur"), callback, {cs1});
        custom->Show(true);
    }
}

#include "kernel/processors/GPU/ColorFIlters/Filters.h"

void MyFrame::OnNegative(wxCommandEvent &event)
{
    using kernel::Processors::GPU::Negative;
    if (workbench->empty())
        wxMessageBox(wxString("Open image first!"),
                     "Info",
                     wxOK | wxICON_INFORMATION,
                     NULL);
    else
        try
        {
            workbench->add_processor(new Negative());
        }
        catch (const std::runtime_error &ex)
        {
            wxMessageBox(wxString(ex.what()),
                         "Info",
                         wxOK | wxICON_INFORMATION,
                         NULL);
        }
}

void MyFrame::OnGamma(wxCommandEvent &event)
{
    using kernel::Processors::GPU::GammaTransform;
    if (workbench->empty())
        wxMessageBox(wxString("Open image first!"),
                     "Info",
                     wxOK | wxICON_INFORMATION,
                     NULL);
    else
    {
        CustomScroll cs1(1, 100, "Gamma param",
                         [](int x) { return 1 + (x - 50) / 100.0; },
                         [](int x) { return std::string("Gamma param: ") + std::to_string(1 + (x - 50) / 100.0); });
        Workbench *wb = this->workbench;
        auto callback = [wb](std::vector<double> args) {
            try
            {
                wb->add_processor(new GammaTransform(args[0]));
            }
            catch (const std::runtime_error &ex)
            {
                wxMessageBox(wxString(ex.what()),
                             "Info",
                             wxOK | wxICON_INFORMATION,
                             NULL);
            }
        };
        FilterAdder *custom = new FilterAdder(wxT("Add gamma transform"), callback, {cs1});
        custom->Show(true);
    }
}

void MyFrame::OnGrayScale(wxCommandEvent &event)
{
    using kernel::Processors::GPU::GrayScale;
    if (workbench->empty())
        wxMessageBox(wxString("Open image first!"),
                     "Info",
                     wxOK | wxICON_INFORMATION,
                     NULL);
    else
        try
        {
            workbench->add_processor(new GrayScale());
        }
        catch (const std::runtime_error &ex)
        {
            wxMessageBox(wxString(ex.what()),
                         "Info",
                         wxOK | wxICON_INFORMATION,
                         NULL);
        }
}

#include "kernel/processors/GPU/Rotator/Rotator.h"

void MyFrame::OnRotation(wxCommandEvent &event)
{
    using kernel::Processors::GPU::Rotator;
    if (workbench->empty())
        wxMessageBox(wxString("Open image first!"),
                     "Info",
                     wxOK | wxICON_INFORMATION,
                     NULL);
    else
    {
        CustomScroll cs1(1, 360, "Rotation angle", [](int x) { return x / 180.0 * 3.1415; }, [](int x) { return std::string("Rotation angle: ") + std::to_string(x); });
        Workbench *wb = this->workbench;
        auto callback = [wb](std::vector<double> args) {
            try
            {
                wb->add_processor(new Rotator(args[0]));
            }
            catch (const std::runtime_error &ex)
            {
                wxMessageBox(wxString(ex.what()),
                             "Info",
                             wxOK | wxICON_INFORMATION,
                             NULL);
            }
        };
        FilterAdder *custom = new FilterAdder(wxT("Rotate image"), callback, {cs1});
        custom->Show(true);
    }
}

#include "kernel/processors/GPU/Rescaler/BicubicRescaler.h"

void MyFrame::OnScale(wxCommandEvent &event)
{
    using kernel::Processors::GPU::BicubicRescaler;
    if (workbench->empty())
        wxMessageBox(wxString("Open image first!"),
                     "Info",
                     wxOK | wxICON_INFORMATION,
                     NULL);
    else
    {
        CustomScroll cs1(1, 30, "X axis ratio", [](int x) { return x / 10.0; }, [](int x) { return std::string("X axis ratio: ") + std::to_string(x / 10.0); });
        CustomScroll cs2(1, 30, "Y axis ratio", [](int x) { return x / 10.0; }, [](int x) { return std::string("Y axis ratio: ") + std::to_string(x / 10.0); });
        Workbench *wb = this->workbench;
        auto callback = [wb](std::vector<double> args) {
            try
            {
                wb->add_processor(new BicubicRescaler(args[0], args[1]));
            }
            catch (const std::runtime_error &ex)
            {
                wxMessageBox(wxString(ex.what()),
                             "Info",
                             wxOK | wxICON_INFORMATION,
                             NULL);
            }
        };
        FilterAdder *custom = new FilterAdder(wxT("Scale image"), callback, {cs1, cs2});
        custom->Show(true);
    }
}

void MyFrame::OnUndo(wxCommandEvent &event)
{
    try
    {
        workbench->undo();
    }
    catch (const std::runtime_error &ex)
    {
        wxMessageBox(wxString("Nothing to undo."),
                     "Info",
                     wxOK | wxICON_INFORMATION,
                     NULL);
    }
    workbench->notify_all();
}
void MyFrame::OnRedo(wxCommandEvent &event)
{
    try
    {
        workbench->redo();
    }
    catch (const std::runtime_error &ex)
    {
        wxMessageBox(wxString("Nothing to redo."),
                     "Info",
                     wxOK | wxICON_INFORMATION,
                     NULL);
    }
    workbench->notify_all();
}

void MyFrame::OnSave(wxCommandEvent &event)
{
    try
    {
        wxString path_to_file;
        wxFileDialog openFileDialog(this, _("Save image"), "", "",
                                    _("BMP files (*.bmp)|*.bmp|JPEG files (*.jpeg, *.jpg)|*.jpeg;*.jpg|PNG files (*.png)|*.png"), wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

        if (openFileDialog.ShowModal() == wxID_CANCEL)
            return;
        path_to_file = openFileDialog.GetPath();
        int index = openFileDialog.GetFilterIndex();
        wxString extS[3] = {_T(".bmp"), _T(".jpeg"), _T(".png")};
        wxBitmapType types[3] = {wxBITMAP_TYPE_BMP, wxBITMAP_TYPE_JPEG, wxBITMAP_TYPE_PNG};
        workbench->save(path_to_file + extS[index], types[index]);
    }
    catch (const std::runtime_error &ex)
    {
        wxMessageBox(wxString("Nothing to save."),
                     "Info",
                     wxOK | wxICON_INFORMATION,
                     NULL);
    }
}

void MyFrame::OnOk(wxCommandEvent &ev)
{
    wxMessageBox(ev.GetString(),
                 "Info",
                 wxOK | wxICON_INFORMATION,
                 NULL);
}

wxBEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_MENU(Minimal_Quit, MyFrame::OnQuit)
    EVT_MENU(Minimal_About, MyFrame::OnAbout)
    EVT_MENU(File_Choose, MyFrame::OnOpen)
    EVT_MENU(Box_Blur_Filter, MyFrame::OnBoxBlur)
    EVT_MENU(Negative_Filter, MyFrame::OnNegative)
    EVT_MENU(GrayScale_Filter, MyFrame::OnGrayScale)
    EVT_MENU(Gamma_Filter, MyFrame::OnGamma)
    EVT_MENU(Rescale_Filter, MyFrame::OnScale)
    EVT_MENU(Rotate_Filter, MyFrame::OnRotation)
    EVT_MENU(Ctrl_Z, MyFrame::OnUndo)
    EVT_MENU(Ctrl_Y, MyFrame::OnRedo)
    EVT_MENU(Save, MyFrame::OnSave)
    EVT_COMMAND(wxID_ANY, OK_EVENT, MyFrame::OnOk)
wxEND_EVENT_TABLE()