#include "MyPanel.h"

MyPanel::MyPanel(wxFrame *parent, ObservableImageSource *image_source_) : wxPanel(parent), image_source(image_source_)
{
}

void MyPanel::paintEvent(wxPaintEvent &evt)
{
    wxClientDC dc(this);
    render(dc);
}


#include "kernel/containers/GPU/RGBAImage/RGBAImage.h"

void MyPanel::render(wxDC &dc)
{
    try
    {
        if (!image_source->empty())
        {
            using namespace UI_PARAMS::Constants;

            kernel::Container::GPU::RGBAImage *img = image_source->get_RGBAImage()->get_ptr_copy();
            Utility::fit_to_screen_size(*img);

            wxBitmap bitmap(Utility::wxImage_from_RGBAImage(*img));
            wxSize current_window_size = get_current_size();

            int x_corner = (current_window_size.GetWidth() - bitmap.GetWidth()) / 2,
                y_corner = (current_window_size.GetHeight() - bitmap.GetHeight() - 60) / 2;

            dc.Clear();
            dc.DrawBitmap(bitmap, x_corner, y_corner);
            delete img;
        }
    }
    catch (const std::runtime_error &ex)
    {
        std::cerr << ex.what() << std::endl;
    }
}

wxSize MyPanel::get_current_size() const
{
    return this->GetParent()->GetSize();
}

BEGIN_EVENT_TABLE(MyPanel, wxPanel)
EVT_PAINT(MyPanel::paintEvent)
END_EVENT_TABLE()