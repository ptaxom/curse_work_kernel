#ifndef GUI_MYPANEL
#define GUI_MYPANEL

#include <wx/wx.h>

#include "kernel/containers/Image/Image.h"

#include "GUI/Workbench/ObservableImageSource.h"
#include "GUI/UI_Parameters/UI_Constants.h"
#include "GUI/Utility/Utility.h"

using kernel::Container::Image;

class MyPanel : public wxPanel
{
public:
  MyPanel(wxFrame *parent, ObservableImageSource *image_source_);

  void paintEvent(wxPaintEvent &evt);

  void render(wxDC &dc);

private:
  wxSize get_current_size() const;

private:
  ObservableImageSource *image_source;
  DECLARE_EVENT_TABLE()
};

#endif