#ifndef GUI_FILTER_ADDER_H
#define GUI_FILTER_ADDER_H

#include <wx/wx.h>
#include <vector>
#include <functional>

#include "GUI/MyDialog/CustomScroll.h"

#include "kernel/processors/GPU/IProcessor.h"

class FilterAdder : public wxDialog
{

    enum
    {
        btnOkId = 555,
        btnCancelID = 556
    };

public:
    FilterAdder(const wxString &title, const std::function<void(std::vector<double>)> &ok_callback_, const std::vector<CustomScroll> &scrolls_)
        : wxDialog(NULL, -1, title, wxPoint(300, 300), wxSize(250, 330), wxDEFAULT_FRAME_STYLE ^ wxRESIZE_BORDER)
        , scrolls(scrolls_), ok_callback(ok_callback_)
    {
        wxPanel *panel = new wxPanel(this, -1);

        wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
        wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

        wxButton *okButton = new wxButton(this, btnOkId, wxT("Ok"),
                                          wxDefaultPosition, wxSize(70, 30));
        wxButton *closeButton = new wxButton(this, btnCancelID, wxT("Close"),
                                             wxDefaultPosition, wxSize(70, 30));

        this->SetSize(250, 200 + 30 * scrolls.size());

        int pos = 0;
        for (const auto &cfg : scrolls)
        {
            int id = 1000 * (pos + 1);
            wxStaticText *label = new wxStaticText(panel, id, wxString(cfg.label.c_str(), wxConvUTF8),
            wxPoint(5, 20 + pos * 60), wxSize(240, 40), 0);
            labels.push_back(label);

            wxSlider *slider = new wxSlider(panel, id+1 , cfg.min, cfg.min, cfg.max, wxPoint(5, 30 + pos * 60), wxSize(240, 40), 0);
            sliders.push_back(slider);
            auto converter = cfg.translate_to_string;

            auto callback = [label, converter, slider](wxCommandEvent &){
                std::string lbl = converter(slider->GetValue());
                wxString new_label(lbl.c_str(), wxConvUTF8);
                label->SetLabel(new_label);
            };

            // Change label on scroll
            Bind(wxEVT_SLIDER, callback, id + 1);
            pos++;
        }

        hbox->Add(okButton, 1);
        hbox->Add(closeButton, 1, wxLEFT, 5);

        vbox->Add(panel, 1);
        vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

        SetSizer(vbox);

        Centre();
        ShowModal();

        Destroy();
    }

private:
    std::function<void(std::vector<double>)> ok_callback;
    std::vector<wxStaticText *> labels;
    std::vector<wxSlider *> sliders;
    std::vector<CustomScroll> scrolls;

    void OnOk(wxCommandEvent &event);
    void OnCancel(wxCommandEvent &event);

    wxDECLARE_EVENT_TABLE();
};

#endif