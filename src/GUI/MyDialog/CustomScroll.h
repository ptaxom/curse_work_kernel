#ifndef GUI_CUSTOM_SCROLL_H
#define GUI_CUSTOM_SCROLL_H

#include <functional>
#include <string>

struct CustomScroll
{
    CustomScroll(int mn, int mx, std::string lbl, const std::function<double(int)> &td, const std::function<std::string(int)> &ts) : min(mn), max(mx), label(lbl), translate_to_double(td), translate_to_string(ts)
    {}

    int min;
    int max;
    std::string label;
    std::function<double(int)> translate_to_double;
    std::function<std::string(int)> translate_to_string;
};


#endif