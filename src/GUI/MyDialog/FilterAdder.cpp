#include "FilterAdder.h"

void FilterAdder::OnOk(wxCommandEvent &event)
{
    std::vector<double> input;
    for(int i = 0; i < sliders.size(); i++)
    {
        double val = scrolls[i].translate_to_double(sliders[i]->GetValue());
        input.push_back(val);
    }
    ok_callback(input);
    Close(true);

}

void FilterAdder::OnCancel(wxCommandEvent &event)
{
    Close(true);
}

wxBEGIN_EVENT_TABLE(FilterAdder, wxDialog)
    EVT_BUTTON(btnOkId, FilterAdder::OnOk)
    EVT_BUTTON(btnCancelID, FilterAdder::OnCancel)
wxEND_EVENT_TABLE()
